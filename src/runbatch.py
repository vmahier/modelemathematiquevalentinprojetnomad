# -*-coding:Utf-8 -*

import sys
import codecs
import datetime


def execfile(path, global_vars=None, local_vars=None):
    with open(path, 'rb') as f:
        code = compile(f.read(), path, 'exec')
        exec(code, global_vars, local_vars)


with open('../Benchmark/runbatch.log', 'wb') as fichier:
    
    debut_programme = datetime.datetime.now()   
    fichier.write("Lancement du programe - {}\n".format(debut_programme).encode("utf-8"))

    instances = ["Instance_1_10_2", "Instance_2_10_2", "Instance_3_10_2", "Instance_4_10_2", "Instance_5_12_3", "Instance_6_12_3", "Instance_7_12_3", "Instance_8_12_3", "Instance_9_15_4", "Instance_10_15_4", "Instance_11_15_4", "Instance_12_15_4"]
    
    #instances = ["Instance_9_15_4"]

    for instance in instances :
    
        debut_instance = datetime.datetime.now()   
        fichier.write("**{} - Début : {}\n".format(instance, debut_instance).encode("utf-8"))
    
        fichier_sortie = open("../Benchmark/{}.log".format(instance), "wb")
        fichier_erreur = open("../Benchmark/{}.err".format(instance), "wb")
   
        sys.stdout = fichier_sortie
        sys.stderr = fichier_erreur
    
        sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
        sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
    
        sys.argv = ["main.py", instance, "8", "21600", "2"]
        execfile("main.py")
    
        sys.stdout.flush()
        sys.stderr.flush()
        
        fichier_sortie.close()
        fichier_erreur.close()
    
    fin_programme = datetime.datetime.now()   
    fichier.write("Fin du programe - {}\n".format(fin_programme).encode("utf-8"))
    fichier.flush()
