# -*-coding:Utf-8 -*


import argparse
import sys
import os.path

from donnee.constructeurinstance import ConstructeurInstanceSansDuplication
from donnee.constructeurinstance import ConstructeurInstanceAvecDuplication
from modele.modele import ModeleSansDuplication
from modele.modele import ModeleAvecDuplication

chemin_fichiers_sources = "../Instances/"
extension_fichiers_requetes = ".node"
extension_fichiers_distances = ".dist"
extension_fichiers_temps = ".time"
extensions = (extension_fichiers_requetes, extension_fichiers_distances, extension_fichiers_temps)
encoding_fichiers = "utf-8"

# Le programme doit se lancer avec le nom de l'instance à traiter
parser = argparse.ArgumentParser()
parser.add_argument("nom_instance", type=str, help="Le nom de l'intance à traiter")
parser.add_argument("nombre_de_vehicule", type=int, help="Le nombre de véhicule")
parser.add_argument("temps_resolution", type=int, help="Le temps maximum alloué au solveur pour résoudre le problème")
parser.add_argument("numero_modele", type=int, help="Choix du modèle")
args = parser.parse_args()

nom_instance = args.nom_instance
nombre_de_vehicule = args.nombre_de_vehicule
temps_resolution = args.temps_resolution
numero_modele = args.numero_modele

# On vérifie que les fichiers de l'instances sont bien présent
for extension in extensions:
    if not os.path.exists(chemin_fichiers_sources + nom_instance + extension):
        sys.stderr.write("Impossible de trouver le fichier {} dans le dossier {}".format(nom_instance + extension_fichiers_distances, chemin_fichiers_sources))
        sys.exit(0) 

# On vérifie le numéro de modèle      
if numero_modele <= 0 or numero_modele > 3:
     sys.stderr.write("Le numero du modèle doit être compris entre 1 et 3")

contenu_fichier_requetes = ""
contenu_fichier_distances = ""
contenu_fichier_temps = ""   
 
# Lecture du fichier requêtes     
with open(chemin_fichiers_sources + nom_instance + extension_fichiers_requetes  , 'rb') as fichier:
    contenu_fichier_requetes = fichier.read().decode(encoding_fichiers)
    
# Lecture du fichier distances  
with open(chemin_fichiers_sources + nom_instance + extension_fichiers_distances  , 'rb') as fichier:
    contenu_fichier_distances = fichier.read().decode(encoding_fichiers)
    
# Lecture du fichier temps  
with open(chemin_fichiers_sources + nom_instance + extension_fichiers_temps  , 'rb') as fichier:
    contenu_fichier_temps = fichier.read().decode(encoding_fichiers)

# Création de l'intance   

if numero_modele==1 :
    constructeur_instance = ConstructeurInstanceSansDuplication(nom_instance,contenu_fichier_requetes, contenu_fichier_distances, contenu_fichier_temps, nombre_de_vehicule)
    instance = constructeur_instance.construire_instance(True) 
    instance.afficher_informations()
elif numero_modele==2:
    constructeur_instance = ConstructeurInstanceSansDuplication(nom_instance,contenu_fichier_requetes, contenu_fichier_distances, contenu_fichier_temps, nombre_de_vehicule)
    instance = constructeur_instance.construire_instance(False) 
    instance.afficher_informations()
else:
   constructeur_instance = ConstructeurInstanceAvecDuplication(nom_instance,contenu_fichier_requetes, contenu_fichier_distances, contenu_fichier_temps, nombre_de_vehicule)
   instance = constructeur_instance.construire_instance(True) 
   instance.afficher_informations()


# Création du modèle 
if numero_modele==1:
    modele = ModeleSansDuplication("resultat_modele_sans_duplication_discretise", instance, True)
    # Résolution du modèle   
    modele.resoudre(temps_resolution)
elif numero_modele==2:
    modele = ModeleSansDuplication("resultat_modele_sans_duplication_continu", instance, False)
    # Résolution du modèle   
    modele.resoudre(temps_resolution)
else:
    modele = ModeleAvecDuplication("resultat_modele_avec_duplication_discretise", instance)
    # Résolution du modèle   
    modele.resoudre(temps_resolution)




