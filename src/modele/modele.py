# -*-coding:Utf-8 -*

import datetime

from gurobipy import Model
from gurobipy.gurobipy import GRB, quicksum

from donnee import instance
from donnee.sommet import Sommet

# Heure minimum (7H00)
U = 25200
# Heure maximum (12H00)
L = 43200
# Durée service maximum (10 min)
S = 600

class Modele:
    
    nom_variable_x_ijK = "X_ij^k"
    nom_variable_y_rk = "Y_r^k"
    nom_variable_z_suffixe = "^h"
    nom_variable_z_preffixe = "Z_"
    nom_variable_l_ik = "L_i^k"
    nom_variable_w_ik = "W_i^k"
    nom_variable_phi_ik = "phi_i^k"
    nom_variable_s_ik = "s_i^k"
    nom_variable_f_i = "f_i"
    nom_variable_g_i = "g_i"
    nom_variable_h_ir = "h_ir"
    
    def __init__(self, nom, instance,discretise):
        self._nom = nom
        self._modele = Model(nom)
        self._instance = instance
        self._discretise = discretise;
        
    def resoudre(self, temps_resolution):
        
        print("Résolution du modèle...")
        debut = datetime.datetime.now()   
        print("[[[[[     Debut - {}     ]]]]]".format(debut))
        self._modele.params.timeLimit = int(temps_resolution)
        self._modele.optimize()
        fin = datetime.datetime.now()   
        print("[[[[[     Fin - {}     ]]]]]".format(fin))
        print("[[[[[     Durée - {}     ]]]]]".format(fin-debut))
        
        self.afficher_solution()
        
    def afficher_solution(self):
        pass

       
class ModeleSansDuplication(Modele):
    
    
    def __init__(self, nom, instance, discretise):
        
        Modele.__init__(self, nom, instance,discretise)
        
        print("Construction du modèle...")
        
        # Ensembles
        R = instance.get_requetes()
        K = instance.get_vehicules()
        P = instance.get_points_collecte()
        D = instance.get_points_livraison()
        V = instance.get_sommets()
        N = P + D
        
        A = []
        for a in instance.generateur_arretes():
            A.append(a)
            
        # -------------------------------------------------------------------------------------------------------
        
        # Création des variables
        
        # Création des x_ij^k
        x_ijK = self._modele.addVars(A, K, vtype=GRB.BINARY, name=Modele.nom_variable_x_ijK)
        
        # Création des y_r^k
        y_rk = self._modele.addVars(R, K, vtype=GRB.BINARY, name=Modele.nom_variable_y_rk)
        
        # Création des l_i^K
        capacite_vehicule = instance.get_capacite_vehicule()
        l_ik = self._modele.addVars(V, K, lb=0, ud=capacite_vehicule, vtype=GRB.CONTINUOUS, name=Modele.nom_variable_l_ik)
        
        # Création des w_i^K
        w_ik = self._modele.addVars(V, K, lb=U, ub=L, vtype=GRB.CONTINUOUS, name=Modele.nom_variable_w_ik)
        
        # Création des phi_i^K
        phi_ik = self._modele.addVars(V, K, lb=-capacite_vehicule, ud=capacite_vehicule, vtype=GRB.CONTINUOUS, name=Modele.nom_variable_phi_ik)
        
        # Création des s_i^K
        s_ik = self._modele.addVars(V, K, lb=0 , ud=S, vtype=GRB.CONTINUOUS, name=Modele.nom_variable_s_ik)
        
        h_ir = self._modele.addVars(A, R, vtype=GRB.BINARY, name=Modele.nom_variable_h_ir)
        
        # -------------------------------------------------------------------------------------------------------
        
        # Création des contraintes
        
        # Contrainte (2) 
        for r in R:
            self._modele.addConstr(quicksum(y_rk[r, k] for k in K) == 1)
       
        # Contrainte (3)        
        for r in R:
            pr = instance.get_point_collecte_requete(r)
            for k in K:
                self._modele.addConstr((y_rk[r, k] - quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet2=pr))) <= 0)
                
        # Contrainte (4)        
        for r in R:
            dr = instance.get_point_livraison_requete(r)
            for k in K:
                self._modele.addConstr((y_rk[r, k] - quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet1=dr))) <= 0)
         
        # Contrainte (5)         
        for p in P:
            for k in K:
                self._modele.addConstr((quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet2=p))) <= (quicksum(y_rk[r, k] for r in R if r.get_numero_point_collecte() == p.get_numero())))
        
        # Contrainte (6)  
        for d in D:
            for k in K:
                self._modele.addConstr((quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet1=d))) <= (quicksum(y_rk[r, k] for r in R  if r.get_numero_point_livraison() == d.get_numero())))
      
        # Contrainte (7)             
        for n in N:
            for k in K:
                self._modele.addConstr((quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet2=n)) - quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet1=n))) == 0)
       
       # Contrainte (8)     
        for r in R:
            pr = instance.get_point_collecte_requete(r)
            dr = instance.get_point_collecte_requete(r)
            for k in K:
                self._modele.addConstr(w_ik[pr, k] <= w_ik[dr, k])
        
        # Contrainte (9)
        for k in K:
            depot_depart = instance.get_depot_depart()
            depot_arrivee = instance.get_depot_arrivee()
            sommets = list(P)
            sommets.append(depot_arrivee)
            self._modele.addConstr(quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet1=depot_depart) if j in sommets) == 1)
        
        # Contrainte (10)
        for k in K:
            depot_depart = instance.get_depot_depart()
            depot_arrivee = instance.get_depot_arrivee()
            sommets = list(D)
            sommets.append(depot_depart)
            self._modele.addConstr(quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet2=depot_arrivee) if i in sommets) == 1)
        
        # Contrainte (11)
        for p in P:
            for k in K:
                self._modele.addConstr(quicksum(y_rk[r, k] * r.get_duree_service_m() for r in R if r.get_numero_point_collecte() == p.get_numero()) == s_ik[p, k])
                
        # Contrainte (12)
        for d in D:
            for k in K:
                self._modele.addConstr(quicksum(y_rk[r, k] * r.get_duree_service_n() for r in R if r.get_numero_point_livraison() == d.get_numero()) == s_ik[d, k])
       
        # Contrainte (13)
        for (i, j) in A:
            
            # Sommet i
            if i.get_type_sommet() == Sommet.type_collecte:
                
                indices_requetes = instance.get_indices_requetes_point_collecte(i.get_numero())
                duree_sommets = instance.get_duree_sommets(i, j)
                max_fenetre_fermeture = U
                sum_mi = 0
                for indice_requete in  indices_requetes:
                    requete = R[indice_requete]
                    duree_service_m = requete.get_duree_service_m()
                    fenetre_b = requete.get_fenetre_b()
                    sum_mi += duree_service_m
                    if fenetre_b > max_fenetre_fermeture:
                        max_fenetre_fermeture = fenetre_b
                
                M = max_fenetre_fermeture + sum_mi + duree_sommets
             
            elif i.get_type_sommet() == Sommet.type_livraison:
                
                indices_requetes = instance.get_indices_requetes_point_livraison(i.get_numero())
                max_fenetre_fermeture = instance.get_maximum_fenetre_fermeture_livraison(i)
                duree_sommets = instance.get_duree_sommets(i, j)
                sum_ni = 0
                for indice_requete in  indices_requetes:
                    requete = R[indice_requete]
                    duree_service_n = requete.get_duree_service_n()
                    sum_ni += duree_service_n
                
                M = max_fenetre_fermeture + sum_ni + duree_sommets 
            
            elif i.get_type_sommet() == Sommet.type_depot_depart:
                
                M = L + 0 + 0
            
            # Sommet j
            if j.get_type_sommet() == Sommet.type_collecte:
                
                indices_requetes = instance.get_indices_requetes_point_collecte(j.get_numero())
                min_fenetre_ouverture = L
                for indice_requete in  indices_requetes:
                    requete = R[indice_requete]
                    fenetre_a = requete.get_fenetre_a()
                    if fenetre_a < min_fenetre_ouverture:
                         min_fenetre_ouverture = fenetre_a
                
                M = M - min_fenetre_ouverture
                
            elif j.get_type_sommet() == Sommet.type_livraison:
                min_fenetre_ouverture = instance.get_minimum_fenetre_ouverture_livraison(j)
                M = M - min_fenetre_ouverture
                
            elif j.get_type_sommet() == Sommet.type_depot_depart:
                 M = M - U
                
            for k in K:
                duree_sommets = instance.get_duree_sommets(i, j)
                self._modele.addConstr(w_ik[j, k] >= (w_ik[i, k] + duree_sommets + s_ik[i, k] - (M * (1 - x_ijK[i, j, k]))))
       
        # Contrainte (14)
        for r in R:
            for k in K:
                fentre_a = r.get_fenetre_a()
                fentre_b = r.get_fenetre_b()
                pr = instance.get_point_collecte_requete(r)
                
                indices_requetes = instance.get_indices_requetes_point_collecte(pr.get_numero())
                max_fenetre_fermeture = U
                for indice_requete in  indices_requetes:
                    requete = R[indice_requete]
                    fenetre_b = requete.get_fenetre_b()
                    if fenetre_b > max_fenetre_fermeture:
                        max_fenetre_fermeture = fenetre_b
                
                N = max_fenetre_fermeture 
                
                self._modele.addConstr(fentre_a * y_rk[r, k] <= w_ik[pr, k]) 
                self._modele.addConstr(w_ik[pr, k] <= fentre_b * y_rk[r, k] + ((1 - y_rk[r, k]) * N))     
       
        #===========================================================================================================
        #===========================================================================================================
        
        if discretise:
            
             # Création des z_h^i
             dic_variables_z_sommet = {}
             for d in D:
                 crenau_horaire = instance.get_h_i_livraison(d.get_numero())
                 dic_variables_z_sommet[d] = self._modele.addVars(crenau_horaire, vtype=GRB.BINARY, name=Modele.nom_variable_z_preffixe + str(d.get_numero()) + Modele.nom_variable_z_suffixe)
            
            # Contrainte (15)        
             for livraison in dic_variables_z_sommet:
                z_ih = dic_variables_z_sommet[livraison] 
                self._modele.addConstr(quicksum(z_ih) == 1)
            
            # Contrainte (16)        
             for livraison in dic_variables_z_sommet:
                z_ih = dic_variables_z_sommet[livraison]
                h_i = instance.get_h_i_livraison(livraison.get_numero())
                crenaux_horraires_livraison = instance.get_crenaux_horraires_livraison(livraison.get_numero())
                for k in K:
                    self._modele.addConstr(quicksum(z_ih[h] * crenaux_horraires_livraison[h][0] for h in h_i) <= w_ik[livraison, k])
                    self._modele.addConstr(w_ik[livraison, k] <= quicksum(z_ih[h] * crenaux_horraires_livraison[h][1] for h in h_i))
        else:
            
             # Création des f_i
             f_i = self._modele.addVars(D, lb=U , ud=L, vtype=GRB.CONTINUOUS, name=Modele.nom_variable_f_i)
             
            # Création des g_i
             g_i = self._modele.addVars(D, lb=U , ud=L, vtype=GRB.CONTINUOUS, name=Modele.nom_variable_g_i)
        
            # Contrainte (15)        
             for d in D:
                for k in K:
                     self._modele.addConstr(f_i[d] <= w_ik[d, k])
                     self._modele.addConstr(w_ik[d, k] <= g_i[d])
                     
             # Contrainte (16)        
             for d in D:
                 identifiant_etablissement = d.get_identifiant()
                 heure_ouverture_min =  instance.get_heure_ouverture_min_etablissement(identifiant_etablissement)
                 self._modele.addConstr(f_i[d] >= heure_ouverture_min)
                 
             # Contrainte (17)        
             for d in D:
                 identifiant_etablissement = d.get_identifiant()
                 heure_fermeture_max =  instance.get_heure_fermeture_max_etablissement(identifiant_etablissement)
                 self._modele.addConstr(g_i[d] <= heure_fermeture_max)
                 
            # Contrainte (18)        
             for d in D:
                 identifiant_etablissement = d.get_identifiant()
                 ecart =  instance.get_ecart_etablissement(identifiant_etablissement)
                 self._modele.addConstr(g_i[d] - f_i[d]  <= ecart)
        
        #===========================================================================================================
        #===========================================================================================================
       
        # Contrainte (17)/(19)
        for p in P:
            for k in K:
                self._modele.addConstr(quicksum(y_rk[r, k] * r.get_nombre_personne_requete() for r in R if r.get_numero_point_collecte() == p.get_numero()) == phi_ik[p, k])
                
        # Contrainte (18)/(20)
        for d in D:
            for k in K:
                self._modele.addConstr((-1 * quicksum(y_rk[r, k] * r.get_nombre_personne_requete() for r in R if r.get_numero_point_livraison() == d.get_numero())) == phi_ik[d, k])
       
        # Contrainte (19)/(21)
        for (i, j) in A:
            O = capacite_vehicule
            for k in K:
                self._modele.addConstr(l_ik[j, k] >= ((l_ik[i, k] + phi_ik[i, k]) - (O * (1 - x_ijK[i, j, k]))))
            
        # Contrainte (20)/(22)
        for v in V:
            for k in K:
                self._modele.addConstr(l_ik[v, k] <= capacite_vehicule)             
          
        # Contrainte (21)/(23)
        for r in R:
            for k in K:
                pr = instance.get_point_collecte_requete(r)
                dr = instance.get_point_livraison_requete(r)
                m = r.get_duree_service_m()
                t = r.get_duree_maximum_requete()
                self._modele.addConstr((w_ik[dr, k] - w_ik[pr, k] - m) * y_rk[r, k] <= t) 
           
        # Contrainte (22)/(24)
        for k in K:
            depot_depart = instance.get_depot_depart()
            depot_arrivee = instance.get_depot_arrivee()
            duree_maximum_trajet = instance.get_duree_maximum_trajet()
            self._modele.addConstr((w_ik[depot_arrivee, k] - w_ik[depot_depart, k]) <= duree_maximum_trajet)
        
        # -------------------------------------------------------------------------------------------------------
        
        # Création de la fonction objectif
        
        depot_depart = instance.get_depot_depart()
        depot_arrivee = instance.get_depot_arrivee()
         
        remuneration_chauffeur = instance.get_remuneration_chauffeur() * quicksum((w_ik[depot_arrivee, k] - w_ik[depot_depart, k]) for k in K)
        
        cout_fixe_vehicule = instance.get_cout_fixe_vehicule() * quicksum(x_ijK[depot_depart, j, k] for j in P for k in K)
        
        cout_distance_vehicule = instance.get_cout_distance_vehicule() * quicksum(instance.get_distances_sommets(i, j) * x_ijK[i, j, k] for (i, j) in A for k in K)
        
        obj = remuneration_chauffeur + cout_fixe_vehicule + cout_distance_vehicule
        
        self._modele.setObjective(obj, GRB.MINIMIZE)
        
    def afficher_solution(self):
        
        if self._modele.solCount == 0 :
            
            print("-----------------------------------------------------------------------------------------------------------")
            print("                                          Aucune solution                                                  ")
            print("-----------------------------------------------------------------------------------------------------------")
        
        else :
            
            # On écrit les valeurs des variables dans un fichier
            #self._modele.write(str(self._nom) + ".sol")
            
            # On affiche la solution trouver
            print("-----------------------------------------------------------------------------------------------------------")
            print("                                               Solution                                                    ")
            print("-----------------------------------------------------------------------------------------------------------")
            
            
            print("----------------------------")
            print("Coût de la solution : ")
            print ("%.2f euros" % self._modele.objVal)
            print("----------------------------")
            
            print("----------------------------")
            print("Utilisation des véhicules : ")
            
            nombre_vehicule = self._instance.get_nombre_de_vehicule()
            print("Nombre de véhicule disponible : {}".format(nombre_vehicule))
            
            points_collecte = self._instance.get_points_collecte()
            indice_depot_depart = self._instance.get_depot_depart().get_numero()
            
            vehicules_utilise = []
            k = 0
            while (k < nombre_vehicule):
                utilise = False
                for collecte in points_collecte:
                    j = collecte.get_numero()
                    variable = round(self._modele.getVarByName(Modele.nom_variable_x_ijK + "[{},{},{}]".format(indice_depot_depart, j, k)).X)
                    if variable == 1:
                        if utilise:
                            assert(False)
                        else:
                            print("* Vehicule {} -> Oui".format(k))
                            vehicules_utilise.append(k)
                            utilise = True
                
                if not utilise:
                    print("* Vehicule {} -> Non".format(k))
                k += 1
           
            print("----------------------------")
            
            print("----------------------------")
            print("Requêtes : ")
            
            for requete in self._instance.get_requetes():
                r = requete.get_indice_requete()
                k = 0
                vehicule = None
                while (k < nombre_vehicule):
                    variable = round(self._modele.getVarByName(Modele.nom_variable_y_rk + "[{},{}]".format(r, k)).X)
                    if variable == 1:
                        if vehicule != None:
                            assert(False)
                        vehicule = k
                        print("* Requête {} -> Véhicule {}".format(r, k))
                    k += 1
            
            print("----------------------------")
            
            print("----------------------------")
            print("Itinéraire des véhicules : ")
            
            valeur_x_ijK = []
            dic_requete_vehicule = {}
            
            i = 0
            while (i < nombre_vehicule):
                liste = []
                valeur_x_ijK.append(liste)
                i += 1
            
            for k in vehicules_utilise:
                for (i, j) in self._instance.generateur_arretes():
                    variable = round(self._modele.getVarByName(Modele.nom_variable_x_ijK + "[{},{},{}]".format(i, j, k)).X)
                    if variable == 1:
                        
                        for x in valeur_x_ijK[k]:
                            if x[0] == i:
                                # On ne peut pas passer deux fois par un même sommet
                                assert(False)

                        valeur_w_ik = round(self._modele.getVarByName(Modele.nom_variable_w_ik + "[{},{}]".format(i, k)).X)
                        valeur_x_ijK[k].append((i, j, valeur_w_ik))
                        
                valeur_x_ijK[k] = sorted(valeur_x_ijK[k], key=lambda colonnes: colonnes[2])
                
                print("* Véhicule {} :".format(k))
                premiere_ligne = True
                nombre_valeur = len(valeur_x_ijK[k])
                compteur = 0
                duree = 0
                personne_vehicule = 0
                capacite_vehicule = self._instance.get_capacite_vehicule()
                depart_anterieur = 0
                
                affichage = 0
                
                for valeur in valeur_x_ijK[k]:
                    if premiere_ligne:
                        debut_service = valeur[2]
                        depart_anterieur = debut_service
                        print("\t", "(({})) //Type : Dépôt de départ//".format(valeur[0].get_numero()), " - (Début du service : {})".format(instance.convertir_duree_en_heure(debut_service)))
                        affichage += 1
                        premiere_ligne = False
                        duree = self._instance.get_duree_sommets(valeur[0], valeur[1])
                    elif compteur + 1 == nombre_valeur:
                        
                        phi_ik = round(self._modele.getVarByName(Modele.nom_variable_phi_ik + "[{},{}]".format(valeur[0], k)).X)
                        personne_vehicule += phi_ik
                        assert(personne_vehicule <= capacite_vehicule)
                        debut_service = valeur[2]
                        arrivee = (depart_anterieur + duree)
                        attente = debut_service - arrivee
                        s_ik = round(self._modele.getVarByName(Modele.nom_variable_s_ik + "[{},{}]".format(valeur[0], k)).X)
                        depart = arrivee + attente + s_ik
                        requetes = self._instance.get_indices_requetes_point_livraison(valeur[0].get_numero())
                        requetes_desservies = []
                        for requete in requetes:
                                valeur_y = round(self._modele.getVarByName(Modele.nom_variable_y_rk + "[{},{}]".format(requete, k)).X)
                                if valeur_y == 1:
                                    dic_requete_vehicule[requete] = (k)
                                    requetes_desservies.append(requete)
                        print("\t[{}] -> ".format(instance.convertir_duree_en_heure(duree)), "(({})) //Type : Point de livraison, Requêtes : {}//".format(valeur[0].get_numero(), requetes_desservies), " - ((Arrivée : [{}], Attente : {}, Début du service : [{}], Durée du service : {}, Départ : [{}], Variation de charges du véhicule : {})".format(instance.convertir_duree_en_heure(arrivee), instance.convertir_duree_en_heure(attente), instance.convertir_duree_en_heure(debut_service), instance.convertir_duree_en_heure(s_ik), instance.convertir_duree_en_heure(depart), phi_ik))
                        affichage += 1
                        
                        debut_service_depot_arrivee = round(self._modele.getVarByName(Modele.nom_variable_w_ik + "[{},{}]".format(valeur[1], k)).X)
                        duree = self._instance.get_duree_sommets(valeur[0], valeur[1])
                        print("\t[{}] -> ".format(instance.convertir_duree_en_heure(duree)), "(({})) //Type : Dépôt arrivée//".format(valeur[1].get_numero()), " - (Fin du service : {})".format(instance.convertir_duree_en_heure(debut_service_depot_arrivee)))
                        affichage += 1
                        
                        assert(debut_service_depot_arrivee - (depart + duree) <= 0.01)
                    else:
                        phi_ik = round(self._modele.getVarByName(Modele.nom_variable_phi_ik + "[{},{}]".format(valeur[0], k)).X)
                        personne_vehicule += phi_ik
                        assert(personne_vehicule <= capacite_vehicule)
                        debut_service = valeur[2]
                        arrivee = (depart_anterieur + duree)
                        attente = debut_service - arrivee
                        s_ik = round(self._modele.getVarByName(Modele.nom_variable_s_ik + "[{},{}]".format(valeur[0], k)).X)
                        depart = arrivee + attente + s_ik
                        depart_anterieur = depart
                        type_sommet = valeur[0].get_type_sommet()
                        
                        chaine_type = ""
                        requetes = []
                        requetes_desservies = []
                        if type_sommet == Sommet.type_collecte:
                            chaine_type = "Point de collecte"
                            requetes = self._instance.get_indices_requetes_point_collecte(valeur[0].get_numero())
                            for requete in requetes:
                                valeur_y = round(self._modele.getVarByName(Modele.nom_variable_y_rk + "[{},{}]".format(requete, k)).X)
                                if valeur_y == 1:
                                    dic_requete_vehicule[requete] = (k)
                                    requetes_desservies.append(requete)
                                
                        if type_sommet == Sommet.type_livraison:
                            if chaine_type == "":
                                chaine_type = "Point de livraison"
                                requetes = self._instance.get_indices_requetes_point_livraison(valeur[0].get_numero())
                            else:
                                " et de livraison"
                                requetes += self._instance.get_requetes_point_livraison(valeur[0].get_numero())
                            for requete in requetes:
                                valeur_y = round(self._modele.getVarByName(Modele.nom_variable_y_rk + "[{},{}]".format(requete, k)).X)
                                if valeur_y == 1:
                                    requetes_desservies.append(requete)
                        print("\t[{}] -> ".format(instance.convertir_duree_en_heure(duree)), "(({})) //Type : {}, Requêtes : {}//".format(valeur[0].get_numero(), chaine_type, requetes_desservies), " - (Arrivée : [{}], Attente : {}, Début du service : [{}], Durée du service : {}, Départ : [{}], Variation de charges du véhicule : {})".format(instance.convertir_duree_en_heure(arrivee), instance.convertir_duree_en_heure(attente), instance.convertir_duree_en_heure(debut_service), instance.convertir_duree_en_heure(s_ik), instance.convertir_duree_en_heure(depart), phi_ik))
                        affichage += 1
                        
                        duree = self._instance.get_duree_sommets(valeur[0], valeur[1])
            
                    compteur += 1
                 
                # Toute les personnes sont descendus du véhicule   
                assert(personne_vehicule == 0)
                
                assert(affichage - 1 == len(valeur_x_ijK[k]))
            
            print("----------------------------")
            
            print("----------------------------")
            print("Fenêtre horaires des requêtes : ")
            
            for requete in self._instance.get_requetes():
                indice_requete = requete.get_indice_requete()
                fenetre_a = requete.get_fenetre_a()
                fenetre_b = requete.get_fenetre_b()
                print("* Requête {} : [{}  :  {}]".format(indice_requete, instance.convertir_duree_en_heure(fenetre_a), instance.convertir_duree_en_heure(fenetre_b)))
                k = dic_requete_vehicule[indice_requete]
                i = self._instance.get_point_collecte_requete(requete)
                debut_service = round(self._modele.getVarByName(Modele.nom_variable_w_ik + "[{},{}]".format(i, k)).X)
                assert(fenetre_a <= debut_service)
                assert(fenetre_b >= debut_service)

            print("----------------------------")
            
            print("----------------------------")
            print("Fenêtre horaires des établissements: ")
            
            for d in self._instance.get_points_livraison():
                
                if self._discretise:
                    
                    creneau_choisi = None
                    nombre_crenau_horaire = self._instance.get_nombre_crenau_horaire_livraison(d)
                    i = 0
                    while(i < nombre_crenau_horaire):
                        h_i = round(self._modele.getVarByName(Modele.nom_variable_z_preffixe + str(d.get_numero()) + Modele.nom_variable_z_suffixe + "[{}]".format(i)).X)
                        if h_i == 1:
                            if  creneau_choisi == None:
                                creneau_choisi = i
                            else:
                                assert(False)
                        i += 1
                
                    requetes = self._instance.get_indices_requetes_point_livraison(d)
                    creneau = self._instance.get_crenau_horaire_i_livraison(d, creneau_choisi)
                    fenetre_a = creneau[0]
                    fenetre_b = creneau[1]
                
                    for requete in requetes:
                         k = dic_requete_vehicule[indice_requete]
                         debut_service = round(self._modele.getVarByName(Modele.nom_variable_w_ik + "[{},{}]".format(d, k)).X)
                         assert(fenetre_a <= debut_service)
                         assert(fenetre_b >= debut_service)
                
                    print("* Sommet {} : [{}  :  {}]".format(d.get_numero(), instance.convertir_duree_en_heure(fenetre_a), instance.convertir_duree_en_heure(fenetre_b)))
                
                else: 
                    
                    f_i = round(self._modele.getVarByName(Modele.nom_variable_f_i + "[{}]".format(d)).X)
                    g_i = round(self._modele.getVarByName(Modele.nom_variable_g_i + "[{}]".format(d)).X)
                    
                    for requete in requetes:
                         k = dic_requete_vehicule[indice_requete]
                         debut_service = round(self._modele.getVarByName(Modele.nom_variable_w_ik + "[{},{}]".format(d, k)).X)
                         assert(f_i <= debut_service)
                         assert(g_i >= debut_service)
                         
                    ecart = self._instance.get_ecart_etablissement(d.get_identifiant())
                    assert(g_i - f_i <= ecart)
                    
                    print("* Sommet {} : [{}  :  {}]".format(d.get_numero(), instance.convertir_duree_en_heure(f_i), instance.convertir_duree_en_heure(g_i)))
            
            print("----------------------------")
    
class ModeleAvecDuplication(Modele):
    
    def __init__(self, nom, instance):
        
        Modele.__init__(self, nom, instance,True)
        
        print("Construction du modèle...")
        
        # Ensembles
        R = instance.get_requetes()
        K = instance.get_vehicules()
        P = instance.get_points_collecte()
        D = instance.get_points_livraison()
        V = instance.get_sommets()
        N = P + D
        E = instance.get_etablissements()
        
        A = []
        for a in instance.generateur_arretes():
            A.append(a)
            
        # -------------------------------------------------------------------------------------------------------
        
        # Création des variables
        
        # Création des x_ij^k
        x_ijK = self._modele.addVars(A, K, vtype=GRB.BINARY, name=Modele.nom_variable_x_ijK)
        
        # Création des y_r^k
        y_rk = self._modele.addVars(R, K, vtype=GRB.BINARY, name=Modele.nom_variable_y_rk)
        
        # Création des l_i^K
        capacite_vehicule = instance.get_capacite_vehicule()
        l_ik = self._modele.addVars(V, K, lb=0, ud=capacite_vehicule, vtype=GRB.CONTINUOUS, name=Modele.nom_variable_l_ik)
        
        # Création des w_i^K
        w_ik = self._modele.addVars(V, K, lb=U, ub=L, vtype=GRB.CONTINUOUS, name=Modele.nom_variable_w_ik)
        
        
        # -------------------------------------------------------------------------------------------------------
        
        # Création des contraintes
        
        # Contrainte (2) 
        for r in R:
            self._modele.addConstr(quicksum(y_rk[r, k] for k in K) == 1)
            
        # Contrainte (3)        
        for r in R:
            for k in K:
                pr = instance.get_point_collecte_requete(r)
                self._modele.addConstr((y_rk[r, k] - quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet2=pr))) == 0)
                
        # Contrainte (4)        
        for r in R:
            pr = instance.get_point_collecte_requete(r)
            dr = instance.get_point_livraison_requete(r)
            for k in K:
                self._modele.addConstr((quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet2=pr)) - quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet1=dr))) == 0)           
        
        # Contrainte (5)
        for n in N:
            for k in K:
                self._modele.addConstr((quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet2=n)) - quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet1=n))) == 0)
        
        
         # Contrainte (6)
        for k in K:
            depot_depart = instance.get_depot_depart()
            depot_arrivee = instance.get_depot_arrivee()
            sommets = list(P)
            sommets.append(depot_arrivee)
            self._modele.addConstr(quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet1=depot_depart) if j in sommets) == 1)
        
       
        # Contrainte (7)
        for k in K:
            depot_depart = instance.get_depot_depart()
            depot_arrivee = instance.get_depot_arrivee()
            sommets = list(D)
            sommets.append(depot_depart)
            self._modele.addConstr(quicksum(x_ijK[i, j, k] for (i, j) in instance.generateur_arretes(sommet2=depot_arrivee) if i in sommets) == 1)
      
       
        # Contrainte (8)     
        for r in R:
            pr = instance.get_point_collecte_requete(r)
            dr = instance.get_point_collecte_requete(r)
            for k in K:
                self._modele.addConstr(w_ik[pr, k] <= w_ik[dr, k])    
                
                
        # Contrainte (9)
        for (i, j) in A:
            
            # Sommet i
            if i.get_type_sommet() == Sommet.type_collecte:
                
                indice_requete = instance.get_indice_requete_point_collecte(i.get_numero())
                duree_sommets = instance.get_duree_sommets(i, j)
                requete = R[indice_requete]
                max_fenetre_fermeture = requete.get_fenetre_b()
                si = i.get_duree_service()
                
                M = max_fenetre_fermeture + si + duree_sommets
             
            elif i.get_type_sommet() == Sommet.type_livraison:
                
                indice_requete = instance.get_indice_requete_point_livraison(i.get_numero())
                max_fenetre_fermeture = instance.get_maximum_fenetre_fermeture_livraison(i)
                duree_sommets = instance.get_duree_sommets(i, j)
                si = i.get_duree_service()
                
                M = max_fenetre_fermeture + si + duree_sommets 
            
            elif i.get_type_sommet() == Sommet.type_depot_depart:
                
                M = L + 0 + 0
            
            # Sommet j
            if j.get_type_sommet() == Sommet.type_collecte:
                
                indice_requete = instance.get_indice_requete_point_collecte(j.get_numero())
                requete = R[indice_requete]
                min_fenetre_ouverture = requete.get_fenetre_a()
                
                M = M - min_fenetre_ouverture
                
            elif j.get_type_sommet() == Sommet.type_livraison:
                min_fenetre_ouverture = instance.get_minimum_fenetre_ouverture_livraison(j)
                M = M - min_fenetre_ouverture
                
            elif j.get_type_sommet() == Sommet.type_depot_depart:
                 M = M - U
                
            for k in K:
                duree_sommets = instance.get_duree_sommets(i, j)
                si = i.get_duree_service()
                self._modele.addConstr(w_ik[j, k] >= (w_ik[i, k] + duree_sommets + si - (M * (1 - x_ijK[i, j, k]))))   
                
        
        # Contrainte (10)
        for r in R:
            for k in K:
                fentre_a = r.get_fenetre_a()
                fentre_b = r.get_fenetre_b()
                pr = instance.get_point_collecte_requete(r)
                self._modele.addConstr(fentre_a <= w_ik[pr, k]) 
                self._modele.addConstr(w_ik[pr, k] <= fentre_b)   
                
         
        #===========================================================================================================
        #===========================================================================================================
        
        
        dic_variables_z_sommet = {}
        for e in E:
            id_sommet = e.get_numero_un_representant()
            crenau_horaire = instance.get_h_i_livraison(id_sommet)
            dic_variables_z_sommet[e] = self._modele.addVars(crenau_horaire, vtype=GRB.BINARY, name=Modele.nom_variable_z_preffixe + e.get_identifiant() + Modele.nom_variable_z_suffixe)
            
        # Contrainte (15)        
        for e in E:
            z_ih = dic_variables_z_sommet[e] 
            self._modele.addConstr(quicksum(z_ih) == 1)
        
       
        # Contrainte (16)
        for e in E: 
            sommets = e.get_representans()
            z_ih = dic_variables_z_sommet[e]
            id_sommet = e.get_numero_un_representant()
            h_i = instance.get_h_i_livraison(id_sommet)
            crenaux_horraires_livraison = instance.get_crenaux_horraires_livraison(id_sommet)
           
            for sommet in sommets:
                for k in K:
                    self._modele.addConstr(quicksum(z_ih[h] * crenaux_horraires_livraison[h][0] for h in h_i) <= w_ik[sommet, k])
                    self._modele.addConstr(w_ik[sommet, k] <= quicksum(z_ih[h] * crenaux_horraires_livraison[h][1] for h in h_i))
                    
                
        #===========================================================================================================
        #===========================================================================================================
        
        # Contrainte (13)
        for (i, j) in A:
            N = capacite_vehicule
            phi_j = instance.calculer_phi(j.get_numero())
            for k in K:
                self._modele.addConstr(l_ik[j, k] >= ((l_ik[i, k] + phi_j) - (N * (1 - x_ijK[i, j, k]))))
        
         # Contrainte (14)
        for v in V:
            for k in K:
                self._modele.addConstr(l_ik[v, k] <= capacite_vehicule)             
          
        # Contrainte (15)
        for r in R:
            for k in K:
                pr = instance.get_point_collecte_requete(r)
                dr = instance.get_point_livraison_requete(r)
                s = pr.get_duree_service()
                t = r.get_duree_maximum_requete()
                self._modele.addConstr((w_ik[dr, k] - w_ik[pr, k] - s) <= t) 
           
        # Contrainte (16)
        for k in K:
            depot_depart = instance.get_depot_depart()
            depot_arrivee = instance.get_depot_arrivee()
            duree_maximum_trajet = instance.get_duree_maximum_trajet()
            self._modele.addConstr((w_ik[depot_arrivee, k] - w_ik[depot_depart, k]) <= duree_maximum_trajet)
            
    
                
        # -------------------------------------------------------------------------------------------------------
        
        # Création de la fonction objectif
        
        depot_depart = instance.get_depot_depart()
        depot_arrivee = instance.get_depot_arrivee()
         
        remuneration_chauffeur = instance.get_remuneration_chauffeur() * quicksum((w_ik[depot_arrivee, k] - w_ik[depot_depart, k]) for k in K)
        
        cout_fixe_vehicule = instance.get_cout_fixe_vehicule() * quicksum(x_ijK[depot_depart, j, k] for j in P for k in K)
        
        cout_distance_vehicule = instance.get_cout_distance_vehicule() * quicksum(instance.get_distances_sommets(i, j) * x_ijK[i, j, k] for (i, j) in A for k in K)
        
        obj = remuneration_chauffeur + cout_fixe_vehicule + cout_distance_vehicule
        
        self._modele.setObjective(obj, GRB.MINIMIZE)   
        
   
    def afficher_solution(self):
        
        if self._modele.solCount == 0 :
            
            print("-----------------------------------------------------------------------------------------------------------")
            print("                                          Aucune solution                                                  ")
            print("-----------------------------------------------------------------------------------------------------------")
        
        else :
            
            # On écrit les valeurs des variables dans un fichier
            #self._modele.write(str(self._nom) + ".sol")
            
            # On affiche la solution trouver
            print("-----------------------------------------------------------------------------------------------------------")
            print("                                               Solution                                                    ")
            print("-----------------------------------------------------------------------------------------------------------")
            
            print("----------------------------")
            print("Coût de la solution : ")
            print ("%.2f euros" % self._modele.objVal)
            print("----------------------------") 
            
            print("----------------------------")
            print("Utilisation des véhicules : ")
            
            nombre_vehicule = self._instance.get_nombre_de_vehicule()
            print("Nombre de véhicule disponible : {}".format(nombre_vehicule))
            
            points_collecte = self._instance.get_points_collecte()
            indice_depot_depart = self._instance.get_depot_depart().get_numero()
            
            vehicules_utilise = []
            k = 0
            while (k < nombre_vehicule):
                utilise = False
                for collecte in points_collecte:
                    j = collecte.get_numero()
                    variable = round(self._modele.getVarByName(Modele.nom_variable_x_ijK + "[{},{},{}]".format(indice_depot_depart, j, k)).X)
                    if variable == 1:
                        if utilise:
                            assert(False)
                        else:
                            print("* Véhicule {} -> Oui".format(k))
                            vehicules_utilise.append(k)
                            utilise = True
                
                if not utilise:
                    print("* Véhicule {} -> Non".format(k))
                k += 1
           
            print("----------------------------")
            
            print("----------------------------")
            print("Requêtes : ")
            
            for requete in self._instance.get_requetes():
                r = requete.get_indice_requete()
                k = 0
                vehicule = None
                while (k < nombre_vehicule):
                    variable = round(self._modele.getVarByName(Modele.nom_variable_y_rk + "[{},{}]".format(r, k)).X)
                    if variable == 1:
                        if vehicule != None:
                            assert(False)
                        vehicule = k
                        print("* Requête {} -> Véhicule {}".format(r, k))
                    k += 1
            
            print("----------------------------")
            print("----------------------------")
            print("Itinéraire des véhicules : ")
            
            valeur_x_ijK = []
            dic_requete_vehicule = {}
            
            i = 0
            while (i < nombre_vehicule):
                liste = []
                valeur_x_ijK.append(liste)
                i += 1
            
            for k in vehicules_utilise:
                for (i, j) in self._instance.generateur_arretes():
                    variable = round(self._modele.getVarByName(Modele.nom_variable_x_ijK + "[{},{},{}]".format(i, j, k)).X)
                    if variable == 1:
                        
                        for x in valeur_x_ijK[k]:
                            if x[0] == i:
                                # On ne peut pas passer deux fois par un même sommet
                                assert(False)

                        valeur_w_ik = round(self._modele.getVarByName(Modele.nom_variable_w_ik + "[{},{}]".format(i, k)).X)
                        valeur_x_ijK[k].append((i, j, valeur_w_ik))
                        
                valeur_x_ijK[k] = sorted(valeur_x_ijK[k], key=lambda colonnes: colonnes[2])
                
                print("* Véhicule {} :".format(k))
                premiere_ligne = True
                nombre_valeur = len(valeur_x_ijK[k])
                compteur = 0
                duree = 0
                personne_vehicule = 0
                capacite_vehicule = self._instance.get_capacite_vehicule()
                depart_anterieur = 0
                
                affichage = 0
                
                for valeur in valeur_x_ijK[k]:
                    if premiere_ligne:
                        debut_service = valeur[2]
                        depart_anterieur = debut_service
                        print("\t", "(({})) //Type : Dépôt de départ//".format(valeur[0].get_numero()), " - (Début du service : {})".format(instance.convertir_duree_en_heure(debut_service)))
                        affichage += 1
                        premiere_ligne = False
                        duree = self._instance.get_duree_sommets(valeur[0], valeur[1])
                    elif compteur + 1 == nombre_valeur:
                        phi_ik = self._instance.calculer_phi(valeur[0].get_numero())
                        personne_vehicule += phi_ik
                        assert(personne_vehicule <= capacite_vehicule)
                        debut_service = valeur[2]
                        arrivee = (depart_anterieur + duree)
                        attente = debut_service - arrivee
                        s_ik = self._instance.get_duree_service_sommet(valeur[0].get_numero())
                        depart = arrivee + attente + s_ik
                        requete = self._instance.get_indice_requete_point_livraison(valeur[0].get_numero())
                        dic_requete_vehicule[requete] = (k)
                        print("\t[{}] -> ".format(instance.convertir_duree_en_heure(duree)), "(({})) //Type : Point de livraison, Requête : [{}]//".format(valeur[0].get_numero(), requete), " - ((Arrivée : [{}], Attente : {}, Début du service : [{}], Durée du service : {}, Départ : [{}], Variation de charges du véhicule : {})".format(instance.convertir_duree_en_heure(arrivee), instance.convertir_duree_en_heure(attente), instance.convertir_duree_en_heure(debut_service), instance.convertir_duree_en_heure(s_ik), instance.convertir_duree_en_heure(depart), phi_ik))
                        affichage += 1
                        
                        debut_service_depot_arrivee = round(self._modele.getVarByName(Modele.nom_variable_w_ik + "[{},{}]".format(valeur[1], k)).X)
                        duree = self._instance.get_duree_sommets(valeur[0], valeur[1])
                        print("\t[{}] -> ".format(instance.convertir_duree_en_heure(duree)), "(({})) //Type : Dépôt arrivée//".format(valeur[1].get_numero()), " - (Fin du service : {})".format(instance.convertir_duree_en_heure(debut_service_depot_arrivee)))
                        affichage += 1
                        
                        assert(debut_service_depot_arrivee - (depart + duree) <= 0.01)
                    else:
                        phi_ik = self._instance.calculer_phi(valeur[0].get_numero())
                        personne_vehicule += phi_ik
                        assert(personne_vehicule <= capacite_vehicule)
                        debut_service = valeur[2]
                        arrivee = (depart_anterieur + duree)
                        attente = debut_service - arrivee
                        s_ik = self._instance.get_duree_service_sommet(valeur[0].get_numero())
                        depart = arrivee + attente + s_ik
                        depart_anterieur = depart
                        type_sommet = valeur[0].get_type_sommet()
                        
                        chaine_type = ""
                        requete
                        if type_sommet == Sommet.type_collecte:
                            chaine_type = "Point de collecte"
                            requete = self._instance.get_indice_requete_point_collecte(valeur[0].get_numero())
                            dic_requete_vehicule[requete] = (k)
                                
                        if type_sommet == Sommet.type_livraison:
                            if chaine_type == "":
                                chaine_type = "Point de livraison"
                                requete = self._instance.get_indice_requete_point_livraison(valeur[0].get_numero())
                            else:
                                " et de livraison"
                                requete += self._instance.get_requete_point_livraison(valeur[0].get_numero())
                        print("\t[{}] -> ".format(instance.convertir_duree_en_heure(duree)), "(({})) //Type : {}, Requêtes : [{}]//".format(valeur[0].get_numero(), chaine_type, requete), " - (Arrivée : [{}], Attente : {}, Début du service : [{}], Durée du service : {}, Départ : [{}], Variation de charges du véhicule : {})".format(instance.convertir_duree_en_heure(arrivee), instance.convertir_duree_en_heure(attente), instance.convertir_duree_en_heure(debut_service), instance.convertir_duree_en_heure(s_ik), instance.convertir_duree_en_heure(depart), phi_ik))
                        affichage += 1
                        
                        duree = self._instance.get_duree_sommets(valeur[0], valeur[1])
            
                    compteur += 1
                 
                # Toute les personnes sont descendus du véhicule   
                assert(personne_vehicule == 0)
                
                assert(affichage - 1 == len(valeur_x_ijK[k]))
            
            print("----------------------------") 
            print("----------------------------")
            print("Fenêtre horaires des requêtes : ")
            
            for requete in self._instance.get_requetes():
                indice_requete = requete.get_indice_requete()
                fenetre_a = requete.get_fenetre_a()
                fenetre_b = requete.get_fenetre_b()
                print("* Requête {} : [{}  :  {}]".format(indice_requete, instance.convertir_duree_en_heure(fenetre_a), instance.convertir_duree_en_heure(fenetre_b)))
                k = dic_requete_vehicule[indice_requete]
                i = self._instance.get_point_collecte_requete(requete)
                debut_service = round(self._modele.getVarByName(Modele.nom_variable_w_ik + "[{},{}]".format(i, k)).X)
                assert(fenetre_a <= debut_service)
                assert(fenetre_b >= debut_service)

            print("----------------------------")   
            
            print("----------------------------")
            print("Fenêtre horaires des établissements: ")
            
            for e in self._instance.get_etablissements():
                 
                creneau_choisi = None
                nombre_crenau_horaire = self._instance.get_nombre_crenau_horaire_etablissement(e)
                i = 0
                while(i < nombre_crenau_horaire):
                    h_i = round(self._modele.getVarByName(Modele.nom_variable_z_preffixe + str(e.get_identifiant()) + Modele.nom_variable_z_suffixe + "[{}]".format(i)).X)
                    if h_i == 1:
                        if  creneau_choisi == None:
                            creneau_choisi = i
                        else:
                            assert(False)
                    i += 1
                
                creneau = self._instance.get_crenau_horaire_i_etablissement(e, creneau_choisi)
                fenetre_a = creneau[0]
                fenetre_b = creneau[1]
                
                print("* Établissements {} : [{}  :  {}]".format(e.get_identifiant(), instance.convertir_duree_en_heure(fenetre_a), instance.convertir_duree_en_heure(fenetre_b)))
            
            print("----------------------------")  