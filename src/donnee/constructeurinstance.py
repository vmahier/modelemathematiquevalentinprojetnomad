# -*-coding:Utf-8 -*

import sys

from donnee.instance import Instance
from donnee.sommet import Sommet
from donnee.requete import Requete
from donnee.etablissement import Etablissement
from donnee.constructeurhoraire import *

global a


class ConstructeurInstance():
    
    indice_point_collecte_requete = 0
    indice_point_livraison_requete = 1
    indice_type_requete = 2
    indice_a_fenetre_horaire_requete = 3
    indice_b_fenetre_horaire_requete = 4
    indice_duree_service_requete = 5
    indice_duree_maximum_requete = 6
    indice_nombre_personne_requete = (8, 9)
    indice_longitude_requete = 10
    indice_latitude_requete = 11
    indice_identifiant_sommet_requete = 12
    indice_adresse_sommet_requete = 13
    
    id_type_collecte_requete = 0
    id_type_livraison_requete = 1
    id_type_depot_requete = 4
   
    separateur_requetes_fichier_requetes = "\n"
    separateur_informations_fichier_requetes = "\t"
    
    separateur_distances_fichier_distances = "\n"
    separateur_informations_fichier_distances = "\t"
    
    separateur_durees_fichier_temps = "\n"
    separateur_informations_fichier_temps = "\t"
    
    capacite_vehicule = 7
    duree_maximum_trajet = 10800
    remuneration_chauffeur = 0.006614
    cout_fixe_vehicule = 50
    cout_distance_vehicule = 0.00017
    
   
    
    def __init__(self, nom_instance, contenu_fichier_requetes, contenu_fichier_distances, contenu_fichier_temps, nombre_de_vehicule):
        
        self._contenu_fichier_requetes = contenu_fichier_requetes
        self._contenu_fichier_distances = contenu_fichier_distances
        self._contenu_fichier_temps = contenu_fichier_temps
        self._nombre_de_vehicule = nombre_de_vehicule
        self._nom_instance = nom_instance
        
    

class ConstructeurInstanceSansDuplication(ConstructeurInstance):
    
    def __init__(self, nom_instance,contenu_fichier_requetes, contenu_fichier_distances, contenu_fichier_temps, nombre_de_vehicule):
        
        ConstructeurInstance.__init__(self, nom_instance,contenu_fichier_requetes, contenu_fichier_distances, contenu_fichier_temps, nombre_de_vehicule)
    
    def construire_instance(self, fenetre_discretisee):
        
        donnee = Instance(self._nom_instance)
        sommets = {}
        requetes = []
        
        print("Construction de l'instance...")
        
        #-----------------SOMMETS ET REQUETES-----------------
        
        print("\tCréation des sommets et des requêtes...")
       
        fichier_requete_split = self._contenu_fichier_requetes.split(ConstructeurInstance.separateur_requetes_fichier_requetes)
       
        # On supprime les lignes d'entêtes
        del fichier_requete_split[:1]
        
        numero_sommet = 0
        
        dic_identifiants_sommets = {}
        requetes_collecte = []
        dic_horraires_livraison = {}
        dic_etablissements = {}
        dic_horraires_etablissements = {}
        dic_duree_service_n = {}
         
        # Création des sommets
        ligne = 3
        for requete in fichier_requete_split:
             
            # On évite les saut de ligne et la dernière ligne du fichier
            if requete != "\r" and len(requete) > 1:
             
                tableau_requete = requete.split(ConstructeurInstance.separateur_informations_fichier_requetes)
                try:
                    identifiant_sommet = tableau_requete[ConstructeurInstance.indice_identifiant_sommet_requete]
                    numero_sommet_fichier = int(tableau_requete[ConstructeurInstance.indice_point_collecte_requete])
                    longitude_sommet = tableau_requete[ConstructeurInstance.indice_longitude_requete]
                    latitude_sommet = tableau_requete[ConstructeurInstance.indice_latitude_requete]
                    adresse_sommet = tableau_requete[ConstructeurInstance.indice_adresse_sommet_requete]
                    type_requete = int(tableau_requete[ConstructeurInstance.indice_type_requete])
                    dic_identifiants_sommets[numero_sommet_fichier] = identifiant_sommet
                    duree_s = int(tableau_requete[ConstructeurInstance.indice_duree_service_requete])
                    
                    # Pour les points de livraisons
                    fenetre_a = int(tableau_requete[ConstructeurInstance.indice_a_fenetre_horaire_requete])
                    fenetre_b = int(tableau_requete[ConstructeurInstance.indice_b_fenetre_horaire_requete])
                    
                    if identifiant_sommet not in sommets:
                        
                        if type_requete == ConstructeurInstance.id_type_collecte_requete:
                            if(identifiant_sommet not in  sommets):
                                sommets[identifiant_sommet] = (Sommet(numero_sommet, identifiant_sommet, Sommet.type_collecte, longitude_sommet, latitude_sommet, adresse_sommet), numero_sommet_fichier)
                                numero_sommet += 1
                           
                        elif type_requete == ConstructeurInstance.id_type_livraison_requete:
                            if(identifiant_sommet in dic_etablissements):
                                dic_etablissements[identifiant_sommet].append(sommets[identifiant_sommet][0])
                                dic_duree_service_n[identifiant_sommet] = duree_s
                            else:
                                sommet = (Sommet(numero_sommet, identifiant_sommet, Sommet.type_livraison, longitude_sommet, latitude_sommet, adresse_sommet), numero_sommet_fichier)
                                sommets[identifiant_sommet] = sommet
                                dic_horraires_livraison[identifiant_sommet] = (fenetre_a, fenetre_b)
                                dic_etablissements[identifiant_sommet] = []
                                dic_etablissements[identifiant_sommet].append(sommet[0])
                                dic_duree_service_n[identifiant_sommet] = duree_s
                                numero_sommet += 1
                           
                        elif type_requete == ConstructeurInstance.id_type_depot_requete:
                            sommets[identifiant_sommet] = (Sommet(numero_sommet, identifiant_sommet, Sommet.type_depot_depart, longitude_sommet, latitude_sommet, adresse_sommet), numero_sommet_fichier)
                            numero_sommet += 1
                            sommets[identifiant_sommet + "_PRIME"] = (Sommet(numero_sommet, identifiant_sommet + "_PRIME", Sommet.type_depot_arrivee, longitude_sommet, latitude_sommet, adresse_sommet), numero_sommet_fichier)
                            numero_sommet += 1
                        else:
                            sys.stderr.write("Le type de requête de la ligne {} est incorrect : {}".format(ligne, requete))
                            sys.exit(0)
                     
                    if type_requete == ConstructeurInstance.id_type_collecte_requete:
                        requetes_collecte.append(tableau_requete)   
               
                except IndexError:
                    sys.stderr.write("La syntaxe de la ligne {} du fichier requête est incorrect (code 1) : {}".format(ligne, requete))
                    sys.exit(0)
                except ValueError:
                    sys.stderr.write("La syntaxe de la ligne {} du fichier requête est incorrect - Impossible de convertir en int (code 1) : {}".format(ligne, requete))
                    sys.exit(0)
                
            ligne += 1
        
        # Création des requêtes
        indice_requete = 0
        for requete in requetes_collecte:
            
            try:
                identifiant_sommet_fichier = requete[ConstructeurInstance.indice_identifiant_sommet_requete]
                point_livraison_fichier = int (requete[ConstructeurInstance.indice_point_livraison_requete])
                fenetre_a = int(requete[ConstructeurInstance.indice_a_fenetre_horaire_requete])
                fenetre_b = int(requete[ConstructeurInstance.indice_b_fenetre_horaire_requete])
                duree_service_m = int(requete[ConstructeurInstance.indice_duree_service_requete])
                duree_maximum_requete = int(requete[ConstructeurInstance.indice_duree_maximum_requete])
                nombre_personne = 0
                for indice in ConstructeurInstance.indice_nombre_personne_requete:
                    nombre_personne += int(requete[indice])
                    nombre_personne_requete = nombre_personne        
                
                # On recupère les id des point de collecte et de livraison
                point_collecte = sommets[identifiant_sommet_fichier][0].get_numero()
                
                identifiant_point_livraison = dic_identifiants_sommets [point_livraison_fichier]
                point_livraison = sommets[identifiant_point_livraison][0].get_numero()
                duree_service_n = dic_duree_service_n[identifiant_point_livraison]
                
                requetes.append(Requete(indice_requete, point_collecte, point_livraison, fenetre_a, fenetre_b, duree_maximum_requete, nombre_personne_requete, duree_service_m, duree_service_n))
                
                indice_requete += 1
                
            except KeyError:
                sys.stderr.write("La syntaxe d'une ligne dans le fichier requête est incorrect (code 2) : {}".format(requete))
                sys.exit(0) 
            except IndexError:
                sys.stderr.write("La syntaxe d'une ligne dans le fichier requête est incorrect (code 2) : {}".format(requete))
                sys.exit(0)
            except ValueError:
                sys.stderr.write("La syntaxe d'une ligne dans le fichier requête est incorrect - Impossible de convertir en int (code 2) : {}".format(requete))
                sys.exit(0)
        
        #-----------------DISTANCES-----------------
        
        print("\tCréation de la matrice des distances...")
        
        nombre_sommets = len(sommets)
        
        # On crée la matrice des distances
        distances = nombre_sommets * [0]
        i = 0
        while(i < nombre_sommets):
            distances[i] = nombre_sommets * [0]
            i += 1
      
        fichier_distance_split = self._contenu_fichier_distances.split(ConstructeurInstance.separateur_distances_fichier_distances)
        # On supprime les lignes d'entêtes
        del  fichier_distance_split[:1]
        
        distances_fichier = []
        
        for distance in fichier_distance_split:
            if distance != "\r" and len(distance) > 1:
                tmp = distance.split(ConstructeurInstance.separateur_informations_fichier_distances)
                del tmp[0]
                distances_fichier.append(tmp)
       
        for cle_sommet_1 in sommets:
                sommet_1 = sommets[cle_sommet_1]
                numero_sommet_1 = sommet_1[0].get_numero()
                representant_sommet_1 = sommet_1[1]
                for cle_sommet_2 in sommets:
                    sommet_2 = sommets[cle_sommet_2]
                    numero_sommet_2 = sommet_2[0].get_numero()
                    representant_sommet_2 = sommet_2[1]
                    
                    try:
                        distance = distances_fichier[representant_sommet_1][representant_sommet_2]
                        distances[numero_sommet_1][numero_sommet_2] = int(distance)   
                    except IndexError:
                        sys.stderr.write("La syntaxe d'une ligne dans le fichier des distances est incorrect (code 3)".format(ligne))
                        sys.exit(0)
                    except ValueError:
                        sys.stderr.write("La syntaxe d'une ligne dans le fichier des distances est incorrect - Impossible de convertir en int (code 3)".format(ligne))
                        sys.exit(0)
        
        #-----------------DUREES-----------------
        
        print("\tCréation de la matrice des durées...")
        
        # On crée la matrice des durees
        durees = nombre_sommets * [0]
        i = 0
        while(i < nombre_sommets):
            durees[i] = nombre_sommets * [0]
            i += 1
        
        fichier_duree_split = self._contenu_fichier_temps.split(ConstructeurInstance.separateur_durees_fichier_temps)
        # On supprime les lignes d'entêtes
        del  fichier_duree_split[:1]
        
        durees_fichier = []
        
        for duree in fichier_duree_split:
            if duree != "\r" and len(duree) > 1:
                tmp = duree.split(ConstructeurInstance.separateur_informations_fichier_temps)
                del tmp[0]
                durees_fichier.append(tmp)
                
        for cle_sommet_1 in sommets:
                sommet_1 = sommets[cle_sommet_1]
                numero_sommet_1 = sommet_1[0].get_numero()
                representant_sommet_1 = sommet_1[1]
                for cle_sommet_2 in sommets:
                    sommet_2 = sommets[cle_sommet_2]
                    numero_sommet_2 = sommet_2[0].get_numero()
                    representant_sommet_2 = sommet_2[1]
                    
                    try:
                        duree = durees_fichier[representant_sommet_1][representant_sommet_2]
                        durees[numero_sommet_1][numero_sommet_2] = int(duree)    
                    except IndexError:
                        sys.stderr.write("La syntaxe d'une ligne dans le fichier des durées est incorrect (code 4)".format(ligne))
                        sys.exit(0)
                    except ValueError:
                        sys.stderr.write("La syntaxe d'une ligne dans le fichier des durées est incorrect - Impossible de convertir en int (code 4)".format(ligne))
                        sys.exit(0)
        
        print("\tCréation de l'instance...")
        
        # On ajoute les sommets dans l'instance
        for cle_sommet in sommets:
            sommet = sommets[cle_sommet][0]
            type_sommet = sommet.get_type_sommet()
           
            if type_sommet == Sommet.type_collecte:
                donnee.ajouter_sommet_collecte(sommet)
            
            elif type_sommet == Sommet.type_livraison:
               
               if fenetre_discretisee :
                    heure_ouverture = dic_horraires_livraison[cle_sommet][0]
                    heure_fermeture = dic_horraires_livraison[cle_sommet][1]
                    horraires = constructeur_horaire_discretisee(heure_ouverture, heure_fermeture)
                    donnee.ajouter_sommet_livraison(sommet, horraires)
                    dic_horraires_etablissements[cle_sommet] = (horraires.get_minimum_fenetre_ouverture(), horraires.get_maximum_fenetre_fermeture(),  ecart_etablissement())
               else:
                    heure_ouverture = dic_horraires_livraison[cle_sommet][0]
                    heure_fermeture = dic_horraires_livraison[cle_sommet][1]
                    horraires = constructeur_horaire_continu(heure_ouverture, heure_fermeture)
                    donnee.ajouter_sommet_livraison(sommet, horraires)
                    dic_horraires_etablissements[cle_sommet] = (horraires.get_minimum_fenetre_ouverture(), horraires.get_maximum_fenetre_fermeture(),  ecart_etablissement())
            
            elif type_sommet == Sommet.type_depot_depart:
                donnee.ajouter_sommet_depot_depart(sommet)
            
            else:
                donnee.ajouter_sommet_depot_arrivee(sommet)
            
        # On ajoute les requêtes dans l'instance
        for requete in requetes:
            donnee.ajouter_requete(requete)
            
        # On ajoute la matrice des distances
        donnee.ajouter_matrice_distances(distances)
        
        # On ajoute la matrice des durees
        donnee.ajouter_matrice_durees(durees)
        
        for cle_etablissement in dic_etablissements:
            representants = dic_etablissements[cle_etablissement]
            heure_ouverture_min = dic_horraires_etablissements[cle_etablissement][0]
            heure_fermeture_max = dic_horraires_etablissements[cle_etablissement][1]
            ecart = dic_horraires_etablissements[cle_etablissement][2]
            etablissement = Etablissement(cle_etablissement, heure_ouverture_min, heure_fermeture_max, ecart, representants)
            donnee.ajouter_etablissement(etablissement)
        
        # On initialise les paramètres
        donnee.initialiser_parametres(ConstructeurInstance.capacite_vehicule, ConstructeurInstance.duree_maximum_trajet, ConstructeurInstance.remuneration_chauffeur, ConstructeurInstance.cout_fixe_vehicule, ConstructeurInstance.cout_distance_vehicule, self._nombre_de_vehicule)
        
        print("Fin de construction de l'instance...")
        
        return donnee
   

class ConstructeurInstanceAvecDuplication(ConstructeurInstance):
    
    def __init__(self, nom_instance,contenu_fichier_requetes, contenu_fichier_distances, contenu_fichier_temps, nombre_de_vehicule):
        
        ConstructeurInstance.__init__(self, nom_instance,contenu_fichier_requetes, contenu_fichier_distances, contenu_fichier_temps, nombre_de_vehicule)
    
    def construire_instance(self, fenetre_discretisee):
        
        donnee = Instance(self._nom_instance)
        sommets = {}
        requetes = []
        
        print("\tCréation des sommets et des requêtes...")
       
        fichier_requete_split = self._contenu_fichier_requetes.split(ConstructeurInstance.separateur_requetes_fichier_requetes)
       
        # On supprime les lignes d'entêtes
        del fichier_requete_split[:1]
        
        numero_sommet = 0
        
        requetes_collecte = []
        dic_horraires_livraison = {}
        indice_depot = 0;
        dic_etablissements = {}
        dic_horraires_etablissements = {}
        
        # Création des sommets
        ligne = 3
        for requete in fichier_requete_split:
             
            # On évite les saut de ligne et la dernière ligne du fichier
            if requete != "\r" and len(requete) > 1:
             
                tableau_requete = requete.split(ConstructeurInstance.separateur_informations_fichier_requetes)
                try:
                    identifiant_sommet = tableau_requete[ConstructeurInstance.indice_identifiant_sommet_requete]
                    numero_sommet_fichier = int(tableau_requete[ConstructeurInstance.indice_point_collecte_requete])
                    longitude_sommet = tableau_requete[ConstructeurInstance.indice_longitude_requete]
                    latitude_sommet = tableau_requete[ConstructeurInstance.indice_latitude_requete]
                    adresse_sommet = tableau_requete[ConstructeurInstance.indice_adresse_sommet_requete]
                    type_requete = int(tableau_requete[ConstructeurInstance.indice_type_requete])
                    duree_s = int(tableau_requete[ConstructeurInstance.indice_duree_service_requete])
                    fenetre_a = int(tableau_requete[ConstructeurInstance.indice_a_fenetre_horaire_requete])
                    fenetre_b = int(tableau_requete[ConstructeurInstance.indice_b_fenetre_horaire_requete])
                        
                    if type_requete == ConstructeurInstance.id_type_collecte_requete:
                        sommets[numero_sommet_fichier] = Sommet(numero_sommet , identifiant_sommet, Sommet.type_collecte, longitude_sommet, latitude_sommet, adresse_sommet, duree_s)
                        requetes_collecte.append(tableau_requete) 
                        numero_sommet += 1
                    elif type_requete == ConstructeurInstance.id_type_livraison_requete:
                        sommet = Sommet(numero_sommet , identifiant_sommet, Sommet.type_livraison, longitude_sommet, latitude_sommet, adresse_sommet, duree_s)
                        sommets[numero_sommet_fichier] = sommet
                        if(identifiant_sommet in dic_etablissements):
                            dic_etablissements[identifiant_sommet].append(sommet)
                        else:
                            dic_horraires_livraison[identifiant_sommet] = (fenetre_a, fenetre_b)
                            dic_etablissements[identifiant_sommet] = []
                            dic_etablissements[identifiant_sommet].append(sommet)
                        numero_sommet += 1
                            
                    elif type_requete == ConstructeurInstance.id_type_depot_requete:
                        sommets[numero_sommet_fichier] = Sommet(numero_sommet , identifiant_sommet, Sommet.type_depot_depart, longitude_sommet, latitude_sommet, adresse_sommet, duree_s)
                        numero_sommet += 1
                        sommets[-1] = Sommet(numero_sommet , identifiant_sommet + "_PRIME", Sommet.type_depot_arrivee, longitude_sommet, latitude_sommet, adresse_sommet, duree_s)
                        numero_sommet += 1
                        indice_depot = numero_sommet_fichier;
                    else:
                        sys.stderr.write("Le type de requête de la ligne {} est incorrect : {}".format(ligne, requete))
                        sys.exit(0)
               
                except IndexError:
                    sys.stderr.write("La syntaxe de la ligne {} du fichier requête est incorrect (code 1) : {}".format(ligne, requete))
                    sys.exit(0)
                except ValueError:
                    sys.stderr.write("La syntaxe de la ligne {} du fichier requête est incorrect - Impossible de convertir en int (code 1) : {}".format(ligne, requete))
                    sys.exit(0)
                
            ligne += 1
        
        # Création des requêtes
        indice_requete = 0
        for requete in requetes_collecte:
            
            try:
                indice_point_collecte_requete = int(requete[ConstructeurInstance.indice_point_collecte_requete])
                indice_point_livraison_requete = int(requete[ConstructeurInstance.indice_point_livraison_requete])
                point_livraison_fichier = int (requete[ConstructeurInstance.indice_point_livraison_requete])
                fenetre_a = int(requete[ConstructeurInstance.indice_a_fenetre_horaire_requete])
                fenetre_b = int(requete[ConstructeurInstance.indice_b_fenetre_horaire_requete])
                duree_maximum_requete = int(requete[ConstructeurInstance.indice_duree_maximum_requete])
                nombre_personne = 0
                for indice in ConstructeurInstance.indice_nombre_personne_requete:
                    nombre_personne += int(requete[indice])
                    nombre_personne_requete = nombre_personne 
                    
                 # On recupère les id des point de collecte et de livraison
                point_collecte = sommets[ indice_point_collecte_requete].get_numero()
                point_livraison = sommets[indice_point_livraison_requete].get_numero()      
                
                requetes.append(Requete(indice_requete, point_collecte, point_livraison, fenetre_a, fenetre_b, duree_maximum_requete, nombre_personne_requete))
                
                indice_requete += 1
                
            except KeyError:
                sys.stderr.write("La syntaxe d'une ligne dans le fichier requête est incorrect (code 2) : {}".format(requete))
                sys.exit(0) 
            except IndexError:
                sys.stderr.write("La syntaxe d'une ligne dans le fichier requête est incorrect (code 2) : {}".format(requete))
                sys.exit(0)
            except ValueError:
                sys.stderr.write("La syntaxe d'une ligne dans le fichier requête est incorrect - Impossible de convertir en int (code 2) : {}".format(requete))
                sys.exit(0)
        
        #-----------------DISTANCES-----------------
        
        print("\tCréation de la matrice des distances...")
        
        nombre_sommets = len(sommets)
        
        # On crée la matrice des distances
        distances = nombre_sommets * [0]
        i = 0
        while(i < nombre_sommets):
            distances[i] = nombre_sommets * [0]
            i += 1
      
        fichier_distance_split = self._contenu_fichier_distances.split(ConstructeurInstance.separateur_distances_fichier_distances)
        # On supprime les lignes d'entêtes
        del  fichier_distance_split[:1]
        
        distances_fichier = []
        
        for distance in fichier_distance_split:
            if distance != "\r" and len(distance) > 1:
                tmp = distance.split(ConstructeurInstance.separateur_informations_fichier_distances)
                del tmp[0]
                distances_fichier.append(tmp)
       
        for cle_sommet_1 in sommets:
                sommet_1 = sommets[cle_sommet_1]
                numero_sommet_1 = sommet_1.get_numero()
                representant_sommet_1 = cle_sommet_1
                if representant_sommet_1 == -1:
                     representant_sommet_1 = indice_depot
                for cle_sommet_2 in sommets:
                    sommet_2 = sommets[cle_sommet_2]
                    numero_sommet_2 = sommet_2.get_numero()
                    representant_sommet_2 = cle_sommet_2
                    if representant_sommet_2 == -1:
                        representant_sommet_2 = indice_depot
                    try:
                        distance = distances_fichier[representant_sommet_1][representant_sommet_2]
                        distances[numero_sommet_1][numero_sommet_2] = int(distance)
                    except IndexError:
                        sys.stderr.write("La syntaxe d'une ligne dans le fichier des distances est incorrect (code 3)".format(ligne))
                        sys.exit(0)
                    except ValueError:
                        sys.stderr.write("La syntaxe d'une ligne dans le fichier des distances est incorrect - Impossible de convertir en int (code 3)".format(ligne))
                        sys.exit(0)
                        
         #-----------------DUREES-----------------
        
        print("\tCréation de la matrice des durées...")
        
        # On crée la matrice des durees
        durees = nombre_sommets * [0]
        i = 0
        while(i < nombre_sommets):
            durees[i] = nombre_sommets * [0]
            i += 1
        
        fichier_duree_split = self._contenu_fichier_temps.split(ConstructeurInstance.separateur_durees_fichier_temps)
        # On supprime les lignes d'entêtes
        del  fichier_duree_split[:1]
        
        durees_fichier = []
        
        for duree in fichier_duree_split:
            if duree != "\r" and len(duree) > 1:
                tmp = duree.split(ConstructeurInstance.separateur_informations_fichier_temps)
                del tmp[0]
                durees_fichier.append(tmp)
                
        for cle_sommet_1 in sommets:
                sommet_1 = sommets[cle_sommet_1]
                numero_sommet_1 = sommet_1.get_numero()
                representant_sommet_1 = cle_sommet_1
                if representant_sommet_1 == -1:
                     representant_sommet_1 = indice_depot
                for cle_sommet_2 in sommets:
                    sommet_2 = sommets[cle_sommet_2]
                    numero_sommet_2 = sommet_2.get_numero()
                    representant_sommet_2 = cle_sommet_2
                    if representant_sommet_2 == -1:
                        representant_sommet_2 = indice_depot
                    try:
                        duree = durees_fichier[representant_sommet_1][representant_sommet_2]
                        durees[numero_sommet_1][numero_sommet_2] = int(duree)    
                    except IndexError:
                        sys.stderr.write("La syntaxe d'une ligne dans le fichier des durées est incorrect (code 4)".format(ligne))
                        sys.exit(0)
                    except ValueError:
                        sys.stderr.write("La syntaxe d'une ligne dans le fichier des durées est incorrect - Impossible de convertir en int (code 4)".format(ligne))
                        sys.exit(0)
        
        print("\tCréation de l'instance...")
        
        # On ajoute les sommets dans l'instance
        for cle_sommet in sommets:
            sommet = sommets[cle_sommet]
            type_sommet = sommet.get_type_sommet()
           
            if type_sommet == Sommet.type_collecte:
                donnee.ajouter_sommet_collecte(sommet)
            
            elif type_sommet == Sommet.type_livraison:
                
                identifiant_sommet = sommet.get_identifiant()
                
                if fenetre_discretisee :
                    heure_ouverture = dic_horraires_livraison[identifiant_sommet][0]
                    heure_fermeture = dic_horraires_livraison[identifiant_sommet][1]
                    horraires = constructeur_horaire_discretisee(heure_ouverture, heure_fermeture)
                    donnee.ajouter_sommet_livraison(sommet, horraires)
                    dic_horraires_etablissements[identifiant_sommet] = (horraires.get_minimum_fenetre_ouverture(), horraires.get_maximum_fenetre_fermeture(),  ecart_etablissement())
                else:
                    heure_ouverture = dic_horraires_livraison[identifiant_sommet][0]
                    heure_fermeture = dic_horraires_livraison[identifiant_sommet][1]
                    horraires = constructeur_horaire_continu(heure_ouverture, heure_fermeture)
                    donnee.ajouter_sommet_livraison(sommet, horraires)
                    dic_horraires_etablissements[identifiant_sommet] = (horraires.get_minimum_fenetre_ouverture(), horraires.get_maximum_fenetre_fermeture(),  ecart_etablissement())
            
            elif type_sommet == Sommet.type_depot_depart:
                donnee.ajouter_sommet_depot_depart(sommet)
            
            else:
                donnee.ajouter_sommet_depot_arrivee(sommet)
            
        # On ajoute les requêtes dans l'instance
        for requete in requetes:
            donnee.ajouter_requete(requete)
            
        # On ajoute la matrice des distances
        donnee.ajouter_matrice_distances(distances)
        
        # On ajoute la matrice des durees
        donnee.ajouter_matrice_durees(durees)
        
        for cle_etablissement in dic_etablissements:
            representants = dic_etablissements[cle_etablissement]
            heure_ouverture_min = dic_horraires_etablissements[cle_etablissement][0]
            heure_fermeture_max = dic_horraires_etablissements[cle_etablissement][1]
            ecart = dic_horraires_etablissements[cle_etablissement][2]
            etablissement = Etablissement(cle_etablissement, heure_ouverture_min, heure_fermeture_max, ecart, representants)
            donnee.ajouter_etablissement(etablissement)
        
        # On initialise les paramètres
        donnee.initialiser_parametres(ConstructeurInstance.capacite_vehicule, ConstructeurInstance.duree_maximum_trajet, ConstructeurInstance.remuneration_chauffeur, ConstructeurInstance.cout_fixe_vehicule, ConstructeurInstance.cout_distance_vehicule, self._nombre_de_vehicule)
        
        print("Fin de construction de l'instance...")
        
        return donnee      
         
