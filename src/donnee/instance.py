# -*-coding:Utf-8 -*

import sys

from donnee.sommet import Sommet


def convertir_duree_en_heure(duree):
        heures = int(duree // 3600)
        minutes = int((duree % 3600) // 60)
        secondes = int((duree % 3600) % 60)
        return     "%02d:%02d:%02d" % (heures, minutes, secondes)


class Instance():
    
    def __init__(self, nom_instance):
        self._requetes = []
        self._sommets = {}
        self._sommets_collecte = []
        self._sommets_livraison = []
        self._sommet_depot_depart = None
        self._sommet_depot_arrivee = None
        self._etablissements = {}
        self._horaires_livraison = {}
        self._distances = []
        self._durees = []
        self._capacite_vehicule = 0
        self._duree_maximum_trajet = 0
        self._remuneration_chauffeur = 0
        self._cout_fixe_vehicule = 0
        self._cout_distance_vehicule = 0
        self._nombre_de_vehicule = 0
        self._nom_instance = nom_instance
    
    def ajouter_sommet_collecte(self, sommet):
        self._sommets[sommet.get_numero()] = sommet
        self._sommets_collecte.append(sommet)
        
    def ajouter_sommet_livraison(self, sommet, horaires):
        self._sommets[sommet.get_numero()] = sommet
        self._sommets_livraison.append(sommet)
        self._horaires_livraison[sommet.get_numero()] = horaires

    def ajouter_sommet_depot_depart(self, sommet):
        if self._sommet_depot_depart != None:
            del self._sommets[self._sommet_depot_depart.get_numero()]
        self._sommet_depot_depart = sommet
        self._sommets[sommet.get_numero()] = sommet
        
    def ajouter_sommet_depot_arrivee(self, sommet):
        if self._sommet_depot_arrivee != None:
            del self._sommets[self._sommet_depot_arrivee.get_numero()]
        self._sommet_depot_arrivee = sommet
        self._sommets[sommet.get_numero()] = sommet
        
    def ajouter_requete(self, requete):
        self._requetes.append(requete)
        
    def ajouter_matrice_distances(self, distances):
        self._distances = distances
        
    def ajouter_matrice_durees(self, durees):
        self._durees = durees
        
    def ajouter_etablissement(self, etablissement):
        self._etablissements[etablissement.get_identifiant()] = etablissement
        
    def initialiser_parametres(self, capacite_vehicule, duree_maximum_trajet, remuneration_chauffeur, cout_fixe_vehicule, cout_distance_vehicule, nombre_de_vehicule):
        self._capacite_vehicule = capacite_vehicule
        self._duree_maximum_trajet = duree_maximum_trajet
        self._remuneration_chauffeur = remuneration_chauffeur
        self._cout_fixe_vehicule = cout_fixe_vehicule
        self._cout_distance_vehicule = cout_distance_vehicule
        self._nombre_de_vehicule = nombre_de_vehicule
        
    def get_nombre_requete(self):
        return len(self._requetes)
    
    def get_nombre_de_vehicule(self):
        return self._nombre_de_vehicule
    
    def get_nombre_sommet(self):
        return len(self._sommets)
    
    def get_nombre_crenau_horaire_livraison(self, livraison):
        try:
            return self._horaires_livraison[livraison.get_numero()].get_nombre_horaires()
        except KeyError:
            sys.stderr.write("Une erreur c'est produite lors de la recherche des crénaux horaire pour un sommet")
            sys.exit(0)
            
    def get_nombre_crenau_horaire_etablissement(self, etablissement):
        try:
            numero_sommet = etablissement.get_numero_un_representant()
            return self._horaires_livraison[numero_sommet].get_nombre_horaires()
        except KeyError:
            sys.stderr.write("Une erreur c'est produite lors de la recherche des crénaux horaire pour un établissement")
            sys.exit(0)
            
    def get_crenau_horaire_i_livraison(self, livraison, i):
        try:
            return self._horaires_livraison[livraison.get_numero()].get_horaire(i)
        except KeyError:
            sys.stderr.write("Une erreur c'est produite lors de la recherche du crénaux horaire i pour un sommet")
            sys.exit(0)
            
    def get_crenau_horaire_i_etablissement(self, etablissement, i):
        try:
            numero_sommet = etablissement.get_numero_un_representant()
            return self._horaires_livraison[numero_sommet].get_horaire(i)
        except KeyError:
            sys.stderr.write("Une erreur c'est produite lors de la recherche du crénaux horaire i pour un établissement")
            sys.exit(0)
            
    def get_minimum_fenetre_ouverture_livraison(self, livraison):
        try:
            return self._horaires_livraison[livraison.get_numero()].get_minimum_fenetre_ouverture()
        except KeyError:
            sys.stderr.write("Une erreur c'est produite lors du calcul du minimum d'ouverture d'un point de livraison")
            sys.exit(0)
            
    def get_maximum_fenetre_fermeture_livraison(self, livraison):
        try:
            return self._horaires_livraison[livraison.get_numero()].get_maximum_fenetre_fermeture()
        except KeyError:
            sys.stderr.write("Une erreur c'est produite lors du calcul du maximum de fermeture d'un point de livraison")
            sys.exit(0)
            
    def get_sommets(self):
        resultat = []
        for cle_sommet in self._sommets:
            resultat.append(self._sommets[cle_sommet])
        return resultat
    
    def get_etablissements(self):
        resultat = []
        for cle_etablissement in self._etablissements:
            resultat.append(self._etablissements[cle_etablissement])
        return resultat
    
    def get_points_collecte(self):
        return list(self._sommets_collecte)
    
    def get_points_livraison(self):
        return list(self._sommets_livraison)
    
    def get_depot_depart(self):
        return self._sommet_depot_depart
    
    def get_depot_arrivee(self):
        return self._sommet_depot_arrivee
    
    def get_requetes(self):
        return list(self._requetes)
    
    def get_vehicules(self):
        # Dans ce problème, la flotte de véhicule est potentiellement infini, donc on considère qu'on a au maximum 1 véhicule par requête
        resultat = []
        nombre_vehicule = self.get_nombre_de_vehicule()
        i = 0
        while(i < nombre_vehicule):
            resultat.append(i)
            i += 1
        return resultat
    
    def get_h_i_livraison(self, livraison):
        resultat = []
        try:
            horaires = self._horaires_livraison[livraison]
            indice = 0
            while(indice < horaires.get_nombre_horaires()):
                resultat.append(indice)
                indice += 1
        except KeyError:
            sys.stderr.write("Une erreur c'est produite lors de la génartion des h_i pour un sommet")
            sys.exit(0)
        return resultat
    
    def get_crenaux_horraires_livraison(self, livraison):
        
        resultat = []
        try:
            horaires = self._horaires_livraison[livraison]
            for horaire in horaires.generateur_Horaires():
                resultat.append(horaire)
        except KeyError:
            sys.stderr.write("Une erreur c'est produite lors de la génartion des crénaux horaire pour un sommet")
            sys.exit(0)
        return resultat
    
    def get_distances_sommets(self, sommet1, sommet2):
        
        try:
            i = sommet1.get_numero()
            j = sommet2.get_numero()
            return self._distances[i][j]
        except IndexError:
            sys.stderr.write("Une erreur c'est produite lors de la recherche de la distance entre deux sommets")
            sys.exit(0)
            
    def get_duree_sommets(self, sommet1, sommet2):
        
        try:
            i = sommet1.get_numero()
            j = sommet2.get_numero()
            return self._durees[i][j]
        except IndexError:
            sys.stderr.write("Une erreur c'est produite lors de la recherche de la durée entre deux sommets")
            sys.exit(0)
            
    def get_capacite_vehicule(self):
        return self._capacite_vehicule
    
    def get_remuneration_chauffeur(self):
        return self._remuneration_chauffeur
    
    def get_cout_fixe_vehicule(self):
        return self._cout_fixe_vehicule
    
    def get_cout_distance_vehicule(self):
        return self._cout_distance_vehicule
    
    def get_point_collecte_requete(self, requete):
        numero_point_collecte = requete.get_numero_point_collecte()
        try:
            return self._sommets[numero_point_collecte]
        except IndexError:
            sys.stderr.write("Une erreur c'est produite lors de la récupération du sommet du point de collecte d'une requête")
            sys.exit(0)
            
    def get_point_livraison_requete(self, requete):
        numero_point_livraison = requete.get_numero_point_livraison()
        try:
            return self._sommets[numero_point_livraison]
        except IndexError:
            sys.stderr.write("Une erreur c'est produite lors de la récupération du sommet du point de livraison d'une requête")
            sys.exit(0)
            
    def get_duree_maximum_trajet(self):
        return self._duree_maximum_trajet
    
    def get_indices_requetes_point_collecte(self, numero_collecte):
        resultat = []
        for requete in self._requetes:
            if requete.get_numero_point_collecte() == numero_collecte:
                resultat.append(requete.get_indice_requete())
        return resultat
    
    def get_indices_requetes_point_livraison(self, numero_livraison):
        resultat = []
        for requete in self._requetes:
            if requete.get_numero_point_livraison() == numero_livraison:
                resultat.append(requete.get_indice_requete())
        return resultat
    
    def get_indice_requete_point_collecte(self, numero_collecte):
        for requete in self._requetes:
            if requete.get_numero_point_collecte() == numero_collecte:
                return requete.get_indice_requete()
        return -1
    
    def get_indice_requete_point_livraison(self, numero_livraison):
        for requete in self._requetes:
            if requete.get_numero_point_livraison() == numero_livraison:
               return requete.get_indice_requete()
        return -1
    
    def get_heure_ouverture_min_etablissement(self, identifiant_etablissement):
        return self._etablissements[identifiant_etablissement].get_heure_ouverture_min()
    
    def get_heure_fermeture_max_etablissement(self, identifiant_etablissement):
        return self._etablissements[identifiant_etablissement].get_heure_fermeture_max()
    
    def get_ecart_etablissement(self, identifiant_etablissement):
        return self._etablissements[identifiant_etablissement].get_ecart()
            
    def generateur_arretes(self, sommet1=None, sommet2=None):
        
        if sommet1 == None and sommet2 == None:
            
            # On généré les arrêtes allant du dépôt de départ aux points de collectes
            for sommet in self._sommets_collecte:
                yield (self._sommet_depot_depart, sommet)
                
            yield (self._sommet_depot_depart, self._sommet_depot_arrivee)
            
            # On généré les arrêtes allant des points de collectes vers les points de livraisons
            for collecte in self._sommets_collecte:
                for livraison in self._sommets_livraison:
                    if collecte != livraison:
                        yield (collecte, livraison)
                        yield (livraison, collecte)
                            
            # On généré les arrêtes allant des points de collectes vers les points de collectes
            for collecte1 in self._sommets_collecte:
                for collecte2 in self._sommets_collecte:
                        if collecte1 != collecte2:
                            yield (collecte1, collecte2)
                           
            # On généré les arrêtes allant des points de livraisons vers les points de livraisons
            for livraison1 in self._sommets_livraison:
                for livraison2 in self._sommets_livraison:
                        if livraison1 != livraison2:
                            yield (livraison1, livraison2)  
                
            # On généré les arrêtes allant des points de collectes vers le dépôt d'arrivée
            for sommet in self._sommets_livraison:
                yield (sommet, self._sommet_depot_arrivee)
                
        elif sommet1 != None and sommet2 != None:
               
            if sommet1 == sommet2:
                
                sys.stderr.write("Une erreur c'est produite lors de la génération des arrêtes - Le sommet1 ne peut pas être égal au sommet2")
                sys.exit(0)
                    
            else:   
                yield (sommet1, sommet2)
            
        else:
                
            if sommet1 != None:
                
                if sommet1.get_type_sommet() == Sommet.type_depot_depart:
                    
                    for collecte in self._sommets_collecte:
                        yield (sommet1, collecte)
                    
                    yield (sommet1, self._sommet_depot_arrivee)
                        
                elif sommet1.get_type_sommet() == Sommet.type_depot_arrivee:
                    
                    sys.stderr.write("Une erreur c'est produite lors de la génération des arrêtes - Le sommet1 ne peut pas être le dépôt d'arrivee")
                    sys.exit(0)
                        
                else:
                    
                    # Type collecte ou livraison
                      
                    for collecte in self._sommets_collecte:
                        if sommet1 != collecte:
                            yield (sommet1, collecte)
                    
                    for livraison in self._sommets_livraison:
                        if sommet1 != livraison:
                            yield (sommet1, livraison)
                    
                    if sommet1.get_type_sommet() == Sommet.type_livraison :
                        yield(sommet1, self._sommet_depot_arrivee)
                    
            else:
                
                if sommet2.get_type_sommet() == Sommet.type_depot_depart:
                    
                    sys.stderr.write("Une erreur c'est produite lors de la génération des arrêtes - Le sommet2 ne peut pas être le dépôt de départ")
                    sys.exit(0)
                    
                elif sommet2.get_type_sommet() == Sommet.type_depot_arrivee:
                    
                    for livraison in self._sommets_livraison:
                        yield (livraison, sommet2)
                 
                    yield (self._sommet_depot_depart, sommet2)
                    
                else:
                    
                    # Type collecte ou livraison
                      
                    for collecte in self._sommets_collecte:
                        if sommet2 != collecte:
                            yield (collecte, sommet2)
                    
                    for livraison in self._sommets_livraison:
                        if sommet2 != livraison:
                            yield (livraison, sommet2)
                            
                    if sommet2.get_type_sommet() == Sommet.type_collecte :
                        yield(self._sommet_depot_depart, sommet2)
    
    def get_duree_service_sommet(self, numero_sommet):
        try:
            return self._sommets[numero_sommet].get_duree_service()
        except IndexError:
            sys.stderr.write("Une erreur c'est produite lors de la récupération de la durée de service d'un sommet")
            sys.exit(0)
            
    def calculer_phi(self, numero_sommet):
         try:
             sommet = self._sommets[numero_sommet]
             if sommet.get_type_sommet() == Sommet.type_collecte:
                indice_requete = self.get_indice_requete_point_collecte(sommet.get_numero())
                requete = self._requetes[indice_requete]
                return requete.get_nombre_personne_requete()
             
             elif sommet.get_type_sommet() == Sommet.type_livraison:
                indice_requete = self.get_indice_requete_point_livraison(sommet.get_numero())
                requete = self._requetes[indice_requete]
                return -1 * requete.get_nombre_personne_requete()
                
             else:
                return 0 
         except IndexError:
            sys.stderr.write("Une erreur c'est produite lors de la récupération de la durée de service d'un sommet")
            sys.exit(0)
    
    def afficher_informations(self):
        
        print("-----------------------------------------------------------------------------------------------------------")
        print("                                              Instance                                                     ")
        print("-----------------------------------------------------------------------------------------------------------")
        
        print("----------------------------")
        print("Liste des sommets :")
        for cle_sommet in self._sommets:
            print(self._sommets[cle_sommet].afficher_informations())
        print("----------------------------") 
        
        print("----------------------------")   
        print("Liste des requêtes :")
        for requete in self._requetes:
            print(requete.afficher_informations())
        print("----------------------------")
        
        print("----------------------------")    
        print("Matrice des distances :")
        nombre_sommets = len(self._sommets)
        
        i = 0
        while i < nombre_sommets:
            j = 0
            while j < nombre_sommets:
                if j + 1 == nombre_sommets:
                    print(self._distances[i][j] , " ")
                else:
                    print(self._distances[i][j], " ", end='')
                j += 1
            i += 1
        
        print("----------------------------")
        
        print("----------------------------")   
        print("Matrice des durées :")
        
        i = 0
        while i < nombre_sommets:
            j = 0
            while j < nombre_sommets:
                if j + 1 == nombre_sommets:
                    print(self._durees[i][j] , " ")
                else:
                    print(self._durees[i][j], " ", end='')
                j += 1
            i += 1
         
        print("----------------------------")
        
        print("----------------------------")   
        print("Créneaux d'ouverture des points de livraisons :")
        
        for cle_sommet in self._horaires_livraison:
            print(self._sommets[cle_sommet].afficher_informations() + ": ", end='')
            print(self._horaires_livraison[cle_sommet].afficher_informations())
        
        print("----------------------------")
        
        print("----------------------------")   
        print("Etablissements:")
        
        for cle_etablissement in self._etablissements:
            print(self._etablissements[cle_etablissement].afficher_informations())
        
        print("----------------------------")
     
    
    def creer_fichiers_pour_JSON(self):
            
        nombre_sommets = self.get_nombre_sommet()
        
        with open("{}.dist".format(self._nom_instance), "wb") as fichier:
             fichier.write("d_ij".encode("utf-8"))
             i = 0
             while i < nombre_sommets - 1:
                 fichier.write("\t{}".format(i).encode("utf-8"))
                 i += 1;
          
             i = 0
             while i < nombre_sommets:
                 if self._sommets[i].get_type_sommet() != Sommet.type_depot_arrivee:
                     fichier.write("\n{}".format(i).encode("utf-8"))
                     j = 0
                     while j < nombre_sommets:
                         if self._sommets[j].get_type_sommet() != Sommet.type_depot_arrivee:
                             fichier.write("\t{}".format(self._distances[i][j]).encode("utf-8"))
                         j += 1
                 i += 1
                 
             fichier.flush()
             fichier.close()
             
        with open("{}.time".format(self._nom_instance), "wb") as fichier:
             fichier.write("t_ij".encode("utf-8"))
             i = 0
             while i < nombre_sommets - 1:
                 fichier.write("\t{}".format(i).encode("utf-8"))
                 i += 1;
          
             i = 0
             while i < nombre_sommets:
                 if self._sommets[i].get_type_sommet() != Sommet.type_depot_arrivee:
                     fichier.write("\n{}".format(i).encode("utf-8"))
                     j = 0
                     while j < nombre_sommets:
                         if self._sommets[j].get_type_sommet() != Sommet.type_depot_arrivee:
                             fichier.write("\t{}".format(self._durees[i][j]).encode("utf-8"))
                         j += 1
                 i += 1
            
             fichier.flush()
             fichier.close()
  