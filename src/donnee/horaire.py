# -*-coding:Utf-8 -*
import sys

from donnee import instance

class HoraireLivraison:
    
    def __init__(self):
        self._horaires = []
        
    def ajouter_horaire (self, heure_ouverture, heure_fermeture):
        self._horaires.append((heure_ouverture, heure_fermeture))
        
    def get_nombre_horaires(self):
        return len(self._horaires)
    
    def get_horaire(self, i):
        try:
            return self._horaires[i]
        except IndexError:
            sys.stderr.write("Une erreur c'est produite lors de la récupération du crénaux horaire i pour un sommet")
            sys.exit(0)
        
    def generateur_Horaires(self):
        for horraire in self._horaires:
            yield horraire
            
    def get_minimum_fenetre_ouverture(self):
        resultat = None
        for horaire in self._horaires:
            if resultat == None:
                resultat = horaire[0]
            else:
                if horaire[0] < resultat:
                    resultat = horaire[0]
        return resultat
    
    def get_maximum_fenetre_fermeture(self):
        resultat = 0
        for horaire in self._horaires:
            if horaire[1] > resultat:
                resultat = horaire[1]
        return resultat
    
    def afficher_informations(self):
        resultat = "Horaires : "
        premier = True
        for horaire in self._horaires:
            if premier:
                resultat += "[{}  :  {}]".format(instance.convertir_duree_en_heure(horaire[0]), instance.convertir_duree_en_heure(horaire[1]))
                premier = False
            else:
                resultat += "    [{}  :  {}]".format(instance.convertir_duree_en_heure(horaire[0]), instance.convertir_duree_en_heure(horaire[1]))
        return resultat
