# -*-coding:Utf-8 -*
from donnee import instance

class Sommet:
    
    type_collecte = 1
    type_livraison = 2
    type_depot_depart = 3
    type_depot_arrivee = 4
    
    def __init__(self, numero, identifiant, type_sommet, longitude, latitude, adresse, duree_service=0):
          
        self._numero = numero
        self._identifiant = identifiant
        self._type_sommet = type_sommet
        self._longitude = longitude
        self._latitude = latitude
        self._adresse = adresse
        self._duree_service = duree_service
     
    def afficher_informations(self):
        return "Sommet {}, Identifiant {}, Type sommet {}, Longitude {}, Latitude {}, Adresse {}, Durée de service s {}".format(self._numero, self._identifiant, self._type_sommet, self._longitude, self._latitude, self._adresse, instance.convertir_duree_en_heure(self._duree_service))
    
    def get_numero(self):
        return self._numero
    
    def get_identifiant(self):
        return self._identifiant
    
    def get_type_sommet(self):
        return self._type_sommet
    
    def get_duree_service(self):
        return self._duree_service
    
    def get_longitude(self):
        return self._longitude
    
    def get_latitude(self):
        return self._latitude
    
    def get_adresse(self):
        return self._adresse
    
    
    def __str__(self):
        return "{}".format(self._numero)
    
