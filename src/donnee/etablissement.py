# -*-coding:Utf-8 -*
from donnee import instance


class Etablissement():
    
    def __init__(self, identifiant, heure_ouverture_min, heure_fermeture_max, ecart, representants):
          
        self._identifiant = identifiant
        self._heure_ouverture_min = heure_ouverture_min
        self._heure_fermeture_max = heure_fermeture_max
        self._ecart = ecart
        self._representants = representants
        
    def get_sommet_est_etablissement(self,numero_sommet):
        for representant in self._representants:
            if representant.get_numero() == numero_sommet:
                return True
        return False
        
    def get_identifiant(self):
        return self._identifiant
    
    def get_heure_ouverture_min(self):
        return self._heure_ouverture_min
    
    def get_heure_fermeture_max(self):
        return self._heure_fermeture_max
    
    def get_ecart(self):
        return self._ecart
    
    def get_numero_un_representant(self):
        try:
            return self._representants[0].get_numero()
        except KeyError:
            sys.stderr.write("Une erreur c'est produite lors de la récupération d'un représentant pour un établissement")
            sys.exit(0)         
            
    def get_representans(self):
        return list(self._representants)
        
    def afficher_informations(self):
        representants = "["
        for representant in self._representants:
            if  representants == "[":
                representants =  representants + str(representant.get_numero()) 
            else:
                 representants =  representants + "," + str(representant.get_numero())    
        representants =  representants + "]"
        return "Identifiant {}, Fenêtre horaire [{} : {}], Représentants {}, Ecart {} ".format(self._identifiant, instance.convertir_duree_en_heure(self._heure_ouverture_min), instance.convertir_duree_en_heure(self._heure_fermeture_max), representants, instance.convertir_duree_en_heure(self._ecart))
    
