# -*-coding:Utf-8 -*
from donnee import instance


class Requete():
    
    def __init__(self, indice_requete, numero_point_collecte, numero_point_livraison, fenetre_a, fenetre_b, duree_maximum_requete, nombre_personne_requete, duree_service_m = 0, duree_service_n = 0):
          
        self._indice_requete = indice_requete
        self._numero_point_collecte = numero_point_collecte
        self._numero_point_livraison = numero_point_livraison
        self._fenetre_a = fenetre_a
        self._fenetre_b = fenetre_b
        self._duree_service_m = duree_service_m
        self._duree_service_n = duree_service_n
        self._duree_maximum_requete = duree_maximum_requete
        self._nombre_personne_requete = nombre_personne_requete
          
    def afficher_informations(self):
        return "Requête {}, Point de collecte {}, Point de livraison {}, Fenêtre horaire point de collecte [{}  :  {}], Durée de service m {}, Durée de service n {}, Durée maximum requête {}, Nombre de personne {} ".format(self._indice_requete, self._numero_point_collecte, self._numero_point_livraison, instance.convertir_duree_en_heure(self._fenetre_a), instance.convertir_duree_en_heure(self._fenetre_b), instance.convertir_duree_en_heure(self._duree_service_m), instance.convertir_duree_en_heure(self._duree_service_n), instance.convertir_duree_en_heure(self._duree_maximum_requete), self._nombre_personne_requete)

    def get_indice_requete(self):
        return self._indice_requete
    
    def get_numero_point_collecte(self):
        return self._numero_point_collecte
    
    def get_numero_point_livraison(self):
        return self._numero_point_livraison
    
    def get_duree_service_m(self):
        return self._duree_service_m
    
    def get_duree_service_n(self):
        return self._duree_service_n
    
    def get_fenetre_a(self):
        return self._fenetre_a
    
    def get_fenetre_b(self):
        return self._fenetre_b
    
    def get_duree_maximum_requete(self):
        return self._duree_maximum_requete
    
    def get_nombre_personne_requete(self):
        return self._nombre_personne_requete

    def __str__(self):
        return "{}".format(self._indice_requete)