# -*-coding:Utf-8 -*

from donnee.horaire import HoraireLivraison

#Constructeur horraires numéro 1

def ecart_etablissement():
        return 900
    

def constructeur_horaire_discretisee(heure_ouverture, heure_fermeture):
    ecart_actuel = heure_fermeture - heure_ouverture
    if (ecart_actuel != 1800):
        sys.stderr.write("La fenêtre horaire d'un établissement n'est pas de 30 minutes")
        sys.exit(0)
  
    horraire = HoraireLivraison()
    
    horraire.ajouter_horaire(heure_ouverture - 900, heure_ouverture)
    horraire.ajouter_horaire(heure_ouverture, heure_ouverture + 900)
    horraire.ajouter_horaire(heure_ouverture + 900, heure_fermeture)
    return horraire


def constructeur_horaire_continu(heure_ouverture, heure_fermeture):
    ecart_actuel = heure_fermeture - heure_ouverture
    if (ecart_actuel != 1800):
        sys.stderr.write("La fenêtre horaire d'un établissement n'est pas de 30 minutes")
        sys.exit(0)
    
    horraire = HoraireLivraison()
    horraire.ajouter_horaire(heure_ouverture - 900, heure_fermeture)
    
    return horraire

"""
def constructeur_horaire_discretisee(heure_ouverture, heure_fermeture):
    ecart_actuel = heure_fermeture - heure_ouverture
    if (ecart_actuel != 1800):
        sys.stderr.write("La fenêtre horaire d'un établissement n'est pas de 30 minutes")
        sys.exit(0)
  
    horraire = HoraireLivraison()
    horraire.ajouter_horaire(heure_ouverture, heure_ouverture+900)
    return horraire
"""